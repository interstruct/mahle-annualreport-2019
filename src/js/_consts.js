import isTouch from './helpers/_detectTouch';

const { detect } = require('detect-browser');

const device = detect();

export const DOM = {
  $win: $(window),
  $body: $('body'),
  $doc: $('document'),
  $htmlBody: $('html, body'),
  $html: $('html')
};

export const CLASSES = {
  active: 'is-active',
  visible: 'is-visible',
  down: 'is-down',
  up: 'is-up',
  touch: 'is-touch',
  noTouch: 'no-touch',
  open: 'is-open',
  overflowed: 'is-overflowed',
  noClick: 'no-click',
};

export const IS_EN = DOM.$html.attr('data-lang') === 'en';

export const ANIMATION_THROOTLE = 500;

export const BREAKPOINTS = {
  desktopL: 1679,
  tablet: 1023,
  mobile: 767
}

export const IS_TOUCH = isTouch();
export const BROWSER = device.name;
export const OS = device.os;
export const IS_IE = BROWSER === 'ie';
export const IS_IOS = device.os === 'iOS';

export const IS_IOS_12 = IS_IOS && device.version.indexOf('12.',0) !== -1;
export const IS_IOS_11 = IS_IOS && device.version.indexOf('11.',0) !== -1;
export const IS_IOS_13 = IS_IOS && device.version.indexOf('13.',0) !== -1;

export const MAX_PHONE_SIZE = 900;

export const IS_PHONE = !!(IS_TOUCH === true && window.innerWidth < MAX_PHONE_SIZE &&  window.innerHeight < MAX_PHONE_SIZE);
export const IS_PHONE_BIG = !!(IS_PHONE && window.innerWidth > BREAKPOINTS.mobile || IS_PHONE && window.innerHeight > BREAKPOINTS.mobile);
export const IS_PHONE_SMALL = IS_PHONE && !IS_PHONE_BIG;

export const IS_TABLET_BIG = IS_TOUCH && window.innerWidth > BREAKPOINTS.tablet && window.innerHeight > BREAKPOINTS.tablet;
export const IS_TABLET = !!(IS_TOUCH === true && IS_PHONE === false);
export const IS_TABLET_SMALL = IS_TABLET && !IS_TABLET_BIG;

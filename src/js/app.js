import 'babel-polyfill';
import 'object-fit-polyfill';
import svg4everybody from 'svg4everybody';
import { debounce } from 'throttle-debounce';

import detectOS from './helpers/_detectOS';
import detectBrowser from './helpers/_detectBrowser';
import updateViewportUnits from './helpers/_updateViewportUnits';
import initScrollTo from './modules/scroller/_scrollTo';
// import initHeroLoader from './modules/_hero-loader';
// import initBanner from './modules/_banner';

import { DOM, CLASSES, IS_TOUCH, IS_PHONE, IS_TABLET, IS_TABLET_BIG } from './_consts';

const checkIsTouch = () => {
  const { touch, noTouch } = CLASSES;

  if (IS_TOUCH) {
    DOM.$html.addClass(touch);
  } else {
    DOM.$html.addClass(noTouch);
  }
};

const checkDevice = () => {
  if(IS_TABLET){
    if(IS_TABLET_BIG){
      DOM.$html.addClass('is-tablet-big');
    }else{
      DOM.$html.addClass('is-tablet');
    }
  }
  if(IS_PHONE){
    DOM.$html.addClass('is-phone');
  }
};

const initHelpers = () => {
  detectOS();
  detectBrowser();
  checkIsTouch();
  checkDevice();
  updateViewportUnits();
  svg4everybody();
 
};

// !!!  all scroll scripts related with scroll should be included in page.js // for access use you can use 
const initModules = () => {
  initScrollTo();
  // initBanner();
};

const resize = () => {
  updateViewportUnits();
};

const readyHandle = () => {
  initHelpers();
  initModules();
  DOM.$win.on('resize orientationchange', debounce(250, resize));
};

// initHeroLoader();
DOM.$win.ready(readyHandle);

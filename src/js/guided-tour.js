import initHeader from './modules/_header';
import backToTopBtn from './modules/scroller/_back-to-top';
import { initGraph } from './modules/graphs/_graph';
import { initInc } from './modules/graphs/_inc';
import initLines from './modules/graphs/_lines';
import initGroupSlider from './modules/graphs/_group-slider';
import initCircleSlider from './modules/graphs/_circle-slider';
import initGraphScroll from './modules/graphs/_graph-scroll';
import { initPageNavigation } from './modules/_page-navigation';
import initHeroScene from './modules/scroller/_guided-tour-hero-scene';
import initModals from './modules/_modal';

import { DOM } from './_consts';

import './modules/scroller/_page-scroller';

const initModules = () => {
  initHeader();

  backToTopBtn.init();
  initGraph();
  initInc();
  initGroupSlider();
  initCircleSlider();
  initLines();
  initGraphScroll();
  initPageNavigation();
  initModals();

};

const initScenes = () => {
  initHeroScene();
};

const readyHandle = () => {
  initModules();
  initScenes();
};

DOM.$win.ready(readyHandle);

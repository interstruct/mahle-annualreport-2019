import { DOM, BROWSER } from '../_consts';

const detectBrowser = () => {
  if(BROWSER === 'firefox') {
    DOM.$body.addClass('is-firefox');
  }else if (BROWSER === 'ie') {
    DOM.$body.addClass('is-ie');
  }else if (BROWSER === 'edge') {
    DOM.$body.addClass('is-edge');
  }else if (BROWSER === 'chrome') {
    DOM.$body.addClass('is-chrome');
  }else if (BROWSER === 'safari') {
    DOM.$body.addClass('is-safari');
  }
}

export default detectBrowser;
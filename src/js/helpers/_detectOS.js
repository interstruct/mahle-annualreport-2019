import { DOM, OS, IS_IOS_12, IS_IOS_11 } from '../_consts';

const detectOS = () => {
  if(OS.indexOf('Windows') !== -1) {
    DOM.$body.addClass('is-windows');
  }else if(OS.indexOf('Mac') !== -1){
    DOM.$body.addClass('is-mac');
  }else if(OS.indexOf('iOS') !== -1){
    DOM.$body.addClass('is-iOS');
    if(IS_IOS_12){
      DOM.$body.addClass('is-iOS-12');
    }
    if(IS_IOS_11){
      DOM.$body.addClass('is-iOS-11');
    }
  }
}

export default detectOS;
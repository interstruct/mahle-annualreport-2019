import { IS_IE } from '../_consts';

const initMsScroll = () => {
  if (IS_IE) {
    document.addEventListener('mousewheel', (event) => {
      event.preventDefault();
      const wd = event.wheelDelta;
      const csp = window.pageYOffset;
      window.scrollTo(0, csp - wd);
    });

  }
}

export default initMsScroll;

import backToTopBtn from './modules/scroller/_back-to-top';
import initHorizontalSlider from './modules/_horizontal-slider';
import initHeader from './modules/_header';
import initHero from './modules/_home-hero';
import initSlider from './modules/_home-slider';
import initThemesScene from './modules/scroller/_home-scene-themes';
import initModals from './modules/_modal';
import initBanner from './modules/scroller/_banner';

import { DOM } from './_consts';

import './modules/scroller/_page-scroller';

const initModules = () => {
  initHeader();
  backToTopBtn.init();
  initHorizontalSlider();
  initModals();
};

const initScenes = () => {
  initHero();
  initBanner();
  initSlider();
  initThemesScene();
};

const readyHandle = () => {

  initModules();
  initScenes();
};

DOM.$win.ready(readyHandle);

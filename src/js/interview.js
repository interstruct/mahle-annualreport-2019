
import backToTopBtn from './modules/scroller/_back-to-top';
import initHorizontalSlider from './modules/_horizontal-slider';
import initHeader from './modules/_header';
import initHeroScene from './modules/scroller/_interview-hero-scene';
import initScenePuzzle from './modules/scroller/_interview-puzzle-scene';
import initModals from './modules/_modal';
import initBanner from './modules/scroller/_banner';


import { DOM } from './_consts';

import './modules/scroller/_page-scroller';

const initModules = () => {
  initHeader();
  initBanner();
  backToTopBtn.init();
  initHorizontalSlider();
  initModals();
};

const initScenes = () => {
  initHeroScene();
  initScenePuzzle();
};

const readyHandle = () => {
  initModules();
  initScenes();
};

DOM.$win.ready(readyHandle);

import { recountPageNavigation } from './_page-navigation';
import backToTopBtn from './scroller/_back-to-top';
import { CLASSES, IS_IE } from '../_consts';
import '../helpers/_easings';

export default () => {
  if(IS_IE) return;
  const dropClass = 'js-dropdown';

  $(`.${dropClass}`).each((i, el) => {
    const $drop = $(el);
    const $btn = $drop.find('.js-dropdown-btn');

    let canClick = true;

    let RAF_INSTANCE;
    const startRAF = () =>  {
      window.SCROLLER.scroll.update();
      RAF_INSTANCE = window.requestAnimationFrame(startRAF);
    };

    const completeAnimationCallback = () => {
      window.cancelAnimationFrame(RAF_INSTANCE);
      window.SCROLLER.scroll.update();
      recountPageNavigation();
      backToTopBtn.recountValues();
      canClick = true;
    };

    const clickHandle = (e) => {
      if(!canClick) return;
      canClick = false;

      const $content = $(e.currentTarget).parents(`.${dropClass}`).find('.js-dropdown-content');

      if($drop.hasClass(CLASSES.open)){
        $drop.removeClass(CLASSES.open);
        startRAF();
        $content.slideUp(400, 'easeInOutCubic', completeAnimationCallback);
      }else{
        $drop.addClass(CLASSES.open);
        startRAF();
        $content.slideDown(400, 'easeInOutCubic', completeAnimationCallback);
      };

    };

    $btn.on('click', clickHandle);

  });
};

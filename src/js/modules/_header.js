import { throttle } from 'throttle-debounce';
import { DOM, CLASSES } from '../_consts';

const $burger = $('.js-burger');
const $menu = $('.js-menu');
const $header = $('.js-header');
let isOpen = false;

// const headerHeight = $header.outerHeight();

const showHeader = () => {
  $header.removeClass(CLASSES.up).addClass(CLASSES.down);
  $header.closest('.header').removeClass(CLASSES.noClick);
};

const hideHeader = () => {
  $header.removeClass(CLASSES.down).addClass(CLASSES.up);
  $header.closest('.header').addClass(CLASSES.noClick);
};

const onClick = () => {
  isOpen = !isOpen;
  $burger.toggleClass(CLASSES.active);
 
  if(isOpen) $menu.show();
  $menu.toggleClass(CLASSES.active);
  DOM.$body.toggleClass('is-header-open');

  if(!isOpen){
   setTimeout(() => {
    // window.SCROLLER.scroll.start();
    $menu.hide();
   }, 450)
  }
};

const delta = 5;
let lastLocoScrollTop = 0;



const onLocoScroll = e => {

  const st = e.scroll.y;
  // Prevent fast scrolling bugs
  if(Math.abs(lastLocoScrollTop - st) <= delta) return;

  if (st > lastLocoScrollTop) {
    hideHeader();
  } else {
    showHeader();
  }
  lastLocoScrollTop = st;
};

const initHeader = () => {
  $menu.hide();
  $burger.on('click', onClick);
  window.SCROLLER.scroll.on('scroll', throttle(400, onLocoScroll));
};

export default initHeader;

import lottie from 'lottie-web';
import { debounce } from 'throttle-debounce';

import isPortrait from '../helpers/_isPortrait';

import { DOM, IS_TOUCH } from "../_consts";

const initIconsAnimation = () => {
  const $hero = $('.js-hero');
  const $animBlock = $hero.find('.js-anim-block');
  const dataHorizontal = $animBlock.attr('data-icon');
  const dataVertical = $animBlock.attr('data-vertical-icon');
  let isPort = isPortrait();
  let data;

  const params = {
    container: $animBlock[0],
    renderer: 'svg',
    loop: true,
    autoplay: false,
    path: data
  };

  const setData = () => {
    if (isPort) {
      data = dataVertical;
    }else{
      data = dataHorizontal;
    };

    params.path = data;
  };

  setData();

  let anim = lottie.loadAnimation(params);

  anim.play();

  const handleChangeScene = () => {
    anim.stop();

    isPort = isPortrait();

    anim.destroy();
    setData();

    anim = lottie.loadAnimation(params);
    anim.play();
  };

  if(IS_TOUCH){
    DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
  }
};

export default initIconsAnimation;

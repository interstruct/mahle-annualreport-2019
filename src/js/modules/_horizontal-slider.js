import Swiper from 'swiper/js/swiper';
import { BREAKPOINTS } from '../_consts';

export default () => {
  let horizontalSlider;
  let isInited = false;

  const initSlider = () => {
    if (isInited === true) return;
    horizontalSlider = new Swiper('.js-horizontal-slider', {
      scrollbar: {
        el: '.js-horizontal-slider-scrollbar',
        draggable: true,
      },
      slidesPerView: 'auto'
    });
    isInited = true;
  }

  const destroySlider = () => {
    if (isInited === false) return;
    horizontalSlider.destroy(true, true);
    isInited = false;
  }

  const mql = window.matchMedia(`(max-width: ${BREAKPOINTS.mobile}px)`);

  if (!mql.matches) initSlider();

  function screenTest(e) {
    if (e.matches) {
      destroySlider();
    } else {
      initSlider();
    }
  }

  mql.addListener(screenTest);
};

import { debounce } from 'throttle-debounce';

import isPortrait from '../helpers/_isPortrait';

import { CLASSES, DOM, IS_TABLET, IS_TABLET_BIG } from '../_consts';

const modalClass = 'js-modal';
const $modalLinks = $('.js-modal-link');
const $closeBtn = $('.js-modal-close');

const initModals = () => {
  const scroller = window.SCROLLER.scroll;
  const openModal = ($modal) => {
    if($modal.hasClass('js-cookie-modal')){
      $modal.addClass(CLASSES.active);
    }else{
      scroller.stop();
      $modal.addClass(CLASSES.active);
      DOM.$body.addClass(CLASSES.overflowed);
    }
  };

  const closeModal = () => {

    DOM.$body.removeClass(CLASSES.overflowed);
    scroller.start();
    $(`.${modalClass}`).removeClass(CLASSES.active);
  };

  const handleModalLinkClick = (e) => {
    e.preventDefault();

    const $link = $(e.currentTarget);
    const targetAttr = $link.attr('data-target');
    const $target = $(`.${modalClass}[data-modal="${targetAttr}"]`);

    openModal($target);
  };

  const handleCloseModal = (e) => {
    const $clickElem = $(e.currentTarget);
    const $modal = $clickElem.parents(modalClass);
    closeModal($modal);
  };

  $modalLinks.on('click', handleModalLinkClick);
  $closeBtn.on('click', handleCloseModal);

  const resize = () => {
    if(!isPortrait()){
      closeModal();
    }
  };

  if(IS_TABLET && !IS_TABLET_BIG){
    DOM.$win.on('orientationchange', debounce(300, resize));
  };

  const openCookieModal = () => {

    const storage = localStorage.getItem('cookie-shown');
    if(storage !== 'true'){
      localStorage.setItem('cookie-shown', 'true')
      setTimeout(() => {
        openModal($('.js-cookie-modal'))
      }, 1000) 
    }


  };
  openCookieModal()

};

export default initModals;
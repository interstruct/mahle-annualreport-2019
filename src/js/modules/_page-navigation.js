import { debounce } from 'throttle-debounce';
import { DOM, IS_PHONE } from '../_consts';

const $pageNavigation = $('.js-page-nav');
const $pageNavigationButtons = $('.js-page-nav-btn');
const $scrollSection = $('.js-scroll-section');
const delta = $('.js-header').outerHeight();

  // eslint-disable-next-line
let scrolledPixels = 0;

let pixelsToScrollFromTop;
let pixelsToScrollFromBottom;
let sectionsData;


const getScrollPixelsFromTop = () => {
  const $scrollTopSection = $('.js-scroll-top-section');
  const pixels = $scrollTopSection.outerHeight();

  return pixels;
};

const getScrollPixelsFromBottom = () => {
  const $scrollSectionsContainer = $('.js-scroll-sections');
  const pixels = $scrollSectionsContainer.height();

  return pixels;
};

const getSectionsData = () => {
  const data = [];

  $scrollSection.each((i, sec) => {
    const sectionTop = $(sec).position().top;
    const sectionBottom = sectionTop + $(sec).outerHeight();
    data.push({
      top: sectionTop,
      bottom: sectionBottom,
      id: $(sec).attr('id')
    });
  });
  return data;
};

const unstickNavigation = () => {
  $pageNavigation.css('margin-top', pixelsToScrollFromTop);
};

const stickNavigation = () => {
  $pageNavigation.css('margin-top', 0);
};

const poseNavigation = st => {
  if (st > pixelsToScrollFromBottom) {
    $pageNavigation.css('margin-top', pixelsToScrollFromBottom - st);
  }
  if (st < pixelsToScrollFromTop) {
    $pageNavigation.css('margin-top', pixelsToScrollFromTop - st);
  }
  if (st > pixelsToScrollFromTop && st < pixelsToScrollFromBottom) {
    stickNavigation();
  }
};

const getCurrentSectionID = st => {
  let sectionID = '';

  sectionsData.forEach(sec => {
    if (st + DOM.$win.height()/2 > sec.top && st < sec.bottom) sectionID = sec.id;
  });

  return sectionID;
};

const onScroll = e => {
  const st = e.scroll.y;
  scrolledPixels = st;

  poseNavigation(st);

  const currentSectionID = getCurrentSectionID(st);
  window.CURRENT_SECTION_ID = currentSectionID;

  $pageNavigationButtons.removeClass('is-active');
  $(`.js-page-nav-btn[data-target-section="${window.CURRENT_SECTION_ID}"]`).addClass('is-active');
};

const onMobileScroll = e => {
  const st = e.scroll.y;
  const currentSectionID = getCurrentSectionID(st);

  window.CURRENT_SECTION_ID = currentSectionID;
};

const onClick = e => {
  const $targetEl = $(e.currentTarget);
  const targetID = $targetEl.data('target-section');
  const $target = $(`#${targetID}`);

  window.CURRENT_SECTION_ID = targetID;
  window.SCROLLER.scroll.scrollTo($target[0], -delta);
};

const recountPageNavigation = () => {
  pixelsToScrollFromTop = getScrollPixelsFromTop();
  pixelsToScrollFromBottom = getScrollPixelsFromBottom();
  sectionsData = getSectionsData();
};

const initPageNavigation = () => {
  if(IS_PHONE) {
    window.setTimeout(() => {
      sectionsData = getSectionsData();
      window.SCROLLER.scroll.on('scroll', onMobileScroll);
    }, 100);

  } else {

    window.setTimeout(() => {

      pixelsToScrollFromTop = getScrollPixelsFromTop();
      pixelsToScrollFromBottom = getScrollPixelsFromBottom();
      sectionsData = getSectionsData();

      unstickNavigation();

      window.SCROLLER.scroll.on('scroll', onScroll);

      $pageNavigationButtons.on('click', onClick);
    }, 100);

    DOM.$win.on('resize', debounce(100, recountPageNavigation));
  }
};

export { initPageNavigation, recountPageNavigation };

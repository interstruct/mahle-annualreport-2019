import { debounce } from 'throttle-debounce';

import { recountPageNavigation } from './_page-navigation';
import backToTopBtn from './scroller/_back-to-top';

import { DOM, IS_TOUCH, IS_IE, BREAKPOINTS, CLASSES } from '../_consts';

const contentBlockClass = 'js-content-block';
const btnsClass = 'js-scheme-btn';
export default () => {
  const $section = $('.js-powertrain-scheme');
  const $contentCol = $section.find('.js-content-col');
  const $imgWrap = $section.find('.js-scheme-img-wrap');
  const $btns = $section.find(`.${btnsClass}`);

  const AMIN_DURATION = IS_IE ? 300 : 400;

  let windowWidth = window.innerWidth;
  let isSmallBehaviour = windowWidth < 1440;
  let isSmallDevice = windowWidth <= BREAKPOINTS.tablet;

  const setImgPos = () => {
    const colPaddingLeft = parseInt($contentCol.css('padding-left'), 10);
    $imgWrap.css('margin-left', `${colPaddingLeft}px`)
  };

  if (isSmallBehaviour && !isSmallDevice) {
    setImgPos();
  };

  const isExistActiveBlocks = () => {
    const $activeBtns = $(`.${btnsClass}.${CLASSES.active}`)
    const isExistActiveBlock = $activeBtns.length;
    return isExistActiveBlock > 0;
  };

  const updateScrollDependencies = () => {
    if (!IS_IE) {
      recountPageNavigation();
      backToTopBtn.recountValues();
      window.SCROLLER.scroll.update();
    }
  };

  $btns.each((i, el) => {
    let canPlay = true;
    const $btn = $(el);
    const index = $btn.attr('data-index');
    const $target = $(`.${contentBlockClass}[data-index="${index}"]`);

    const completeAnimationCallback = () => {
      canPlay = true;
      updateScrollDependencies();
    };

    const activeHandle = () => {
      $btn.addClass(CLASSES.active);
      $target.slideDown(AMIN_DURATION, 'easeInOutCubic', () => {
        canPlay = true;
        updateScrollDependencies();
        if(isSmallDevice){
          setTimeout(() => {
            const delta = $('.js-header').outerHeight() / 2;
            window.SCROLLER.scroll.scrollTo($contentCol[0], -delta);
          }, 100);
        }
      });
    };

    const disactiveHandle = () => {
      $btn.removeClass(CLASSES.active);
      $target.slideUp(AMIN_DURATION, 'easeInOutCubic', completeAnimationCallback);
    };

    const hideAndPlay = (prevActiveIndex) => {
      $(`.${btnsClass}[data-index="${prevActiveIndex}"]`).removeClass(CLASSES.active);
      $(`.${contentBlockClass}[data-index="${prevActiveIndex}"]`).slideUp(AMIN_DURATION, 'easeInOutCubic', activeHandle);
    };

    const clickHandle = () => {
      if (!canPlay) return;
      canPlay = false;

      if ($btn.hasClass(CLASSES.active)) {
        disactiveHandle();

        if (isSmallBehaviour) {
          setTimeout(() => {
            $imgWrap.removeClass('is-left-pos');
          }, 150)
        }
      } else {
        if (isSmallBehaviour) {
          $imgWrap.addClass('is-left-pos');
        }

        const $activeBtns = $(`.${btnsClass}.${CLASSES.active}`)
        if (!isExistActiveBlocks()) {
          activeHandle()
        } else {
          const prevActiveIndex = $activeBtns.attr('data-index')
          hideAndPlay(prevActiveIndex);
        }
      }
    };

    $btn.on('click', clickHandle);
  });

  const handleChangeScene = () => {
    windowWidth = window.innerWidth
    isSmallDevice = windowWidth <= BREAKPOINTS.tablet;

    isSmallBehaviour = windowWidth < 1440;
    if (isSmallBehaviour) {
      if (isExistActiveBlocks()) {
        $imgWrap.addClass('is-left-pos')
      }
      setImgPos();
    }
    else {
      $imgWrap.removeAttr('style');
      $imgWrap.removeClass('is-left-pos')
    };

    window.SCROLLER.scroll.update();
  }

  if (IS_TOUCH) {
    DOM.$win.on('orientationchange', debounce(400, handleChangeScene));
  } else {
    DOM.$win.on('resize', debounce(400, handleChangeScene));
  };
};
import { CLASSES, IS_TOUCH } from '../_consts';

const toggleTooltip = e => {
  const $target = $(e.currentTarget);
  const $targetTooltip = $target.closest('.js-tooltip-container').find('.js-tooltip');
  const $targetInfoline = $($target.closest('.js-car-box').find('.js-infoline')[$target.closest('.js-tooltip-container').index()]);

  $('.js-tooltip').not($targetTooltip).removeClass(CLASSES.active);
  $('.js-tooltip-button').not($target).removeClass(CLASSES.active);
  $('.js-infoline').not($targetInfoline).removeClass(CLASSES.active);

  $targetTooltip.toggleClass(CLASSES.active);
  $target.toggleClass(CLASSES.active);
  $targetInfoline.toggleClass(CLASSES.active);
};

const initTooltips = () => {
  if (!IS_TOUCH) return;

  const $button = $('.js-tooltip-button');
  $button.on('click', toggleTooltip);
};

export default initTooltips;

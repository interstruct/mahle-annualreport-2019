const initCircle = c => {
  const currentCircle = c;
  const radius = currentCircle.r.baseVal.value;
  const circumference = radius * 2 * Math.PI;
  // const percent = $(currentCircle).closest('.js-circle').data('percent');
  // const circleOffset = circumference - percent / 100 * circumference;

  currentCircle.style.strokeDasharray = `${circumference} ${circumference}`;
  currentCircle.style.strokeDashoffset = circumference;
}

const setProgress = c => {
  const currentCircle = c;
  const radius = currentCircle.r.baseVal.value;
  const circumference = radius * 2 * Math.PI;
  const percent = $(currentCircle).closest('.js-circle').data('percent');
  const circleOffset = circumference - percent / 100 * circumference;

  currentCircle.style.strokeDashoffset = circleOffset;
}

const resetProgress = c => {
  const currentCircle = c;

  $(currentCircle).removeAttr('style');
}

export { initCircle, setProgress, resetProgress };

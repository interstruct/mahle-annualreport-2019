import { throttle } from 'throttle-debounce';
import { resetInc } from './_inc';
import { animateStroke } from './_graph';

import { CLASSES, IS_IE } from '../../_consts';

let lastSectionID = '';

const checkNumber = section => {
  return !!section.find('.js-graph-numb')[0];
};

const checkIsNumbersAnimated = section => {
  return !!section.find('.js-graph-numb').hasClass(CLASSES.active);
};

const animateNumber = section => {
  section.find('.js-graph-numb').addClass(CLASSES.active);
  resetInc();
};

const checkGraph = section => {
  return !!section.find('.js-graph')[0];
};

const checkIsGraphAnimated = section => {
  return !!section.find('.js-graph').hasClass(CLASSES.active);
};

const animateGraph = section => {
  section.find('.js-graph').addClass(CLASSES.active);
};

const checkBlocks = section => {
  return !!section.find('.js-graph-block')[0];
};

const checkIsBlocksAnimated = section => {
  return !!section.find('.js-graph-block').hasClass(CLASSES.active);
};

const animateBlock = (index, block) => {
  window.setTimeout(() => {
    $(block).addClass(CLASSES.active);
  }, 200*index);
};

const animateBlocks = section => {
  section.find('.js-graph-block').each((index, block) => {
    animateBlock(index, block);
  });
};

const checkLines = section => {
  return !!section.find('.js-line')[0];
};

const checkIsLinesAnimated = section => {
  return !!section.find('.js-line').hasClass(CLASSES.active);
};

const animateLine = (index, line) => {
  window.setTimeout(() => {
    $(line).addClass(CLASSES.active);
  }, 200*index);
};

const animateLines = section => {
  section.find('.js-line').each((index, block) => {
    animateLine(index, block);
  });
};

const checkCircles = section => {
  return !!section.find('.js-circle-in')[0];
};

const checkIsCirclesAnimated = section => {
  return !!section.find('.js-circle-in').hasClass(CLASSES.active);
};

const animateCircles = section => {
  section.find('.js-circle-in').each((index, circle) => {
    const currentCircle = circle;
    const radius = currentCircle.r.baseVal.value;
    const circumference = radius * 2 * Math.PI;
    const percent = $(currentCircle).closest('.js-circle').data('percent');
    const circleOffset = circumference - percent / 100 * circumference;

    window.setTimeout(() => {
      currentCircle.style.strokeDashoffset = circleOffset;
    }, 500);
  });
};

const checkInfo = section => {
  return !!section.find('.js-graph-info')[0];
};

const checkIsInfoAnimated = section => {
  return !!section.find('.js-graph-info').hasClass(CLASSES.active);
};

const animateInfo = section => {
  section.find('.js-graph-info').addClass(CLASSES.active);
};


const onScroll = () => {
  if (lastSectionID === window.CURRENT_SECTION_ID) return;

  const currentID = window.CURRENT_SECTION_ID;
  if (!currentID) return;

  const $currentSection = $(`#${currentID}`);
  const HAS_NUMBER = checkNumber($currentSection);
  const IS_NUMBERS_ANIMATED = checkIsNumbersAnimated($currentSection);

  const HAS_GRAPH = checkGraph($currentSection);
  const IS_GRAPH_ANIMATED = checkIsGraphAnimated($currentSection);

  const HAS_BLOCKS = checkBlocks($currentSection);
  const IS_BLOCKS_ANIMATED = checkIsBlocksAnimated($currentSection);

  const HAS_LINES = checkLines($currentSection);
  const IS_LINES_ANIMATED = checkIsLinesAnimated($currentSection);

  const HAS_CIRCLES = checkCircles($currentSection);
  const IS_CIRCLES_ANIMATED = checkIsCirclesAnimated($currentSection);

  const HAS_INFO = checkInfo($currentSection);
  const IS_INFO_ANIMATED = checkIsInfoAnimated($currentSection);

  if (HAS_NUMBER && !IS_NUMBERS_ANIMATED) animateNumber($currentSection);

  if (HAS_GRAPH && !IS_GRAPH_ANIMATED && !IS_IE) animateGraph($currentSection);
  if (HAS_GRAPH && !IS_GRAPH_ANIMATED && IS_IE) animateStroke($currentSection);

  if (HAS_BLOCKS && !IS_BLOCKS_ANIMATED) animateBlocks($currentSection);

  if (HAS_LINES && !IS_LINES_ANIMATED) animateLines($currentSection);

  if (HAS_CIRCLES && !IS_CIRCLES_ANIMATED) animateCircles($currentSection);

  if (HAS_INFO && !IS_INFO_ANIMATED) animateInfo($currentSection);

  lastSectionID = window.CURRENT_SECTION_ID;
};


const initGraphScroll = () => {
  window.SCROLLER.scroll.on('scroll', throttle(200, onScroll));
};

export default initGraphScroll;

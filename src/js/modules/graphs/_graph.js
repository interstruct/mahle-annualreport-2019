import { debounce } from 'throttle-debounce';
import { DOM } from '../../_consts';

const initGraphByValues = graph => {

  const $graphElement = $(graph);
  const $graphPolyline = $graphElement.find('.js-graph-polyline');
  const $graphPolygon = $graphElement.find('.js-graph-polygon');
  const $graphLine = $graphElement.find('.js-graph-line');

  const values = $graphElement[0] ? $graphElement.data('values').split(',') : [];
  const min = Math.min(...values);
  const max = Math.max(...values);

  const minPointY = 6;
  const maxPointY = min < 0 ? $graphElement.height() - 10 : 84;
  const minPointX = 1;
  let maxPointX;


  const calcCurrentPoints = (current) => {
    const diffLength = max - min;
    const diffPointsLength = maxPointY - minPointY;
    const currentLength = max - current;
    // diffLength - 1
    // currentLength - x
    const delta = currentLength*1/diffLength;
    return Math.floor(diffPointsLength*delta+minPointY);
  }

  const drawPolyline = () => {
    let pointsStrPolyline = '';
    let pointsStrPolygon = '';

    maxPointX = $graphElement.width()-1;

    values.forEach((v, index) => {
      let x; let y;

      if (index === 0) {
        x = minPointX;
      } else if (index === 1) {
        x = maxPointX/2;
      } else {
        x = maxPointX;
      }

      if (+v === max) {
        y = minPointY;
      } else if (+v === min) {
        y = maxPointY;
      } else {
        y = calcCurrentPoints(v);
      }

      pointsStrPolyline+= `${x} ${y} `;
      pointsStrPolygon+= `${x} ${y-4} `;
    });

    $graphLine.attr('x1', 0);
    $graphLine.attr('x2', $graphElement.width());
    $graphLine.attr('y1', $graphElement.height());
    $graphLine.attr('y2', $graphElement.height());
    $graphPolyline.attr('points', pointsStrPolyline);
    $graphPolygon.attr('points', `${minPointX} ${$graphElement.height()-5} ${pointsStrPolygon} ${maxPointX} ${$graphElement.height()-5} `);
  };



  drawPolyline();
  DOM.$win.on('resize orientationchange', debounce(200, drawPolyline));

};

const doSetTimeout = (offset, el) => {
  setTimeout(() => {
    el.css('strokeDashoffset', 1500 - offset);
  }, 2.5 * offset);
};

const animateStroke = el => {
  for (let i = 1500; i > 0; i--) {
    doSetTimeout(i, el.find('.js-graph-polyline'));
  }
};

// const hideStroke = el => {
//   el.css('strokeDashoffset', 1500);
// };

const initGraph = () => {
  $('.js-graph').each((index, graph) => {
    initGraphByValues(graph);
  });
};

export { initGraph, animateStroke };

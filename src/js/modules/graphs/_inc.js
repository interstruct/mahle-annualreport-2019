import { animateStroke } from './_graph';
import { setProgress, resetProgress } from './_circles';
import { IS_IE, IS_EN } from '../../_consts';

const numberWithCommas = (x) => {
  return x.toString().replace('.', ',');
};

function Inc(obj) {
  const { elem } = obj;
  let value = parseFloat(elem.getAttribute('data-inc-value')) || 0;
  let duration = parseInt(elem.getAttribute('data-inc-duration'), 10) || 0;
  let delay = parseInt(elem.getAttribute('data-inc-delay'), 10) || 0;
  const decimal = ((obj.decimal > 3) ? 3 : obj.decimal) || 0;
  const currency = obj.currency || '';
  const before = obj.before || '';
  const speed = ((obj.speed < 30) ? 30 : obj.speed) || 30;
  let count = 0;
  let increment = value / (duration / speed);
  let interval = null;
  const regex = /\B(?=(\d{3})+(?!\d))/g;
  const run = function() {
    count += increment;
    if (count < value) {
      if(IS_EN){
        const temp = numberWithCommas((count).toFixed(decimal))
        elem.innerHTML = before + temp + currency;
      }else{
        elem.innerHTML = before + (count).toFixed(decimal).toString().replace(regex, ',') + currency;
      }
    } else {
      clearInterval(interval);
      if(IS_EN){
        const temp = numberWithCommas((value).toFixed(decimal))
        elem.innerHTML = before + temp + currency;
      }else{
        elem.innerHTML = before + (value).toFixed(decimal).toString().replace(regex, ',') + currency;
      }
    }
  };
  setTimeout(function() {
    interval = setInterval(run.bind(this), speed);
  }.bind(this), delay);
  this.reset = function() {
    clearInterval(interval);
    value = parseFloat(elem.getAttribute('data-inc-value')) || 0;
    duration = parseInt(elem.getAttribute('data-inc-duration'), 10) || 0;
    increment = value / (duration / speed);
    delay = parseInt(elem.getAttribute('data-inc-delay'), 10) || 0;
    count = 0;
    interval = setInterval(run, speed);
  };
}

const $elems = $('.js-graph-figure');
const objs = [];

const createInc = () => {
  $elems.each((index, el) => {
    const isInteger = Number.isInteger(+$(el).data('inc-value'));
    const dec = isInteger ? 0 : 3;
    const cur = $(el).data('inc-currency');
    const bef = $(el).data('inc-before');

    objs.push(
      new Inc({
        elem: el,
        speed: 50,
        decimal: dec,
        currency: cur,
        before: bef
      })
    );
  });
};

const resetInc = () => {
  objs.forEach(o => o.reset());
};

const animateLines = (index, block) => {
  window.setTimeout(() => {
    $(block).toggleClass('is-active');
  }, 200*index);
};

const animateLine = (index, line) => {
  window.setTimeout(() => {
    $(line).toggleClass('is-active');
  }, 200*index);
};

const initInc = () => {
  if ($elems.length === 0) return;
  createInc();

  $('.js-anim-trigger').on('click', e => {
    const $section = $(e.target).closest('.graph-section');
    const $blocks = $section.find('.js-graph-block');
    const $lines = $section.find('.js-line');

    $section
      .find('.js-graph-numb')
      .toggleClass('is-active');
    $section
      .find('.js-graph')
      .toggleClass('is-active');

    $blocks.each((index, block) => {
      animateLines(index, block);
    });

    $lines.each((index, line) => {
      animateLine(index, line);
    });

    resetInc();

    $('.js-circle-in').each((i, c) => resetProgress(c));
    $('.js-circle-in').each((i, c) => setProgress(c));

    if (IS_IE && $section.find('.js-graph').hasClass('is-active')) animateStroke($section.find('.js-graph-polyline'));
  });

};

export {initInc, resetInc};

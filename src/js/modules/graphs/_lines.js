import { BREAKPOINTS } from '../../_consts';

const getLinesData = lines => {
  const linesArray = [];
  lines.each((index, line) => {
    linesArray.push({
      el: line,
      value: $(line).data('value')
    });
  });

  return linesArray;
};

const getValues = arr => {
  const valuesArray = [];
  arr.forEach(el => valuesArray.push(el.value));

  return valuesArray;
};

const calcMiddleWidth = props => {
  const diffLength = props.max_val - props.min_val;
  const diffWidthLength = props.max_w - props.min_w;
  const currentLength = props.max_val - props.middle_val;
  // diffLength - 1
  // currentLength - x
  const delta = currentLength*1/diffLength;
  return Math.floor(diffWidthLength*delta+props.min_w);
};

const initLinesByValues = (lines, lineCssValue) => {

  const $linesEl = $(lines);
  const $lines = $linesEl.find('.js-line');
  const linesData = getLinesData($lines);

  const values = getValues(linesData);
  const min = Math.min(...values);
  const max = Math.max(...values);
  const middle = values.filter(v => v !== min && v !== max);

  const IS_NEGATIVE = min < 0;

  const calcs = {
    min_val: min,
    max_val: max,
    middle_val: middle,
    min_w: 65,
    max_w: 100
  };

  if (IS_NEGATIVE) $linesEl.addClass('has-negative-value');

  linesData.forEach(line => {
    $(line.el).attr('style', '');

    if (IS_NEGATIVE) {

      if (line.value === min) {
        $(line.el).css(lineCssValue, `${calcs.min_w/2}%`);
      } else if (line.value === max) {
        $(line.el).css(lineCssValue, `${calcs.max_w/2}%`);
      } else {
        $(line.el).css(lineCssValue, `${calcMiddleWidth(calcs)/5}%`); // /2 - nature value, 5 for hot fix
      }

    } else if (line.value === min) {
        $(line.el).css(lineCssValue, `${calcs.min_w}%`);
      } else if (line.value === max) {
        $(line.el).css(lineCssValue, `${calcs.max_w}%`);
      } else {
        $(line.el).css(lineCssValue, `${calcMiddleWidth(calcs)}%`);
      }

    if (line.value < 0) {
      $(line.el).addClass('is-negative');
      $(line.el).css('bottom', `${50 - calcs.min_w/2}%`);
    }
  });
};

const initLines = () => {
  $('.js-lines').each((index, lines) => {

    let lineCssValue = 'height';

    const mql = window.matchMedia(`(max-width: ${BREAKPOINTS.tablet}px)`);
    if (!mql.matches) lineCssValue = 'height';

    function screenTest(e) {
      if (e.matches) {
        lineCssValue = 'height';
        initLinesByValues(lines, lineCssValue);
      } else {
        lineCssValue = 'height';
        initLinesByValues(lines, lineCssValue);
      }
    }

    mql.addListener(screenTest);
    screenTest(mql);

  });
};

export default initLines;

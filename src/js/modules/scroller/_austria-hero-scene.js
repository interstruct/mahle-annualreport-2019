import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0, Power3 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';

import { CLASSES, IS_PHONE, IS_TABLET, IS_TABLET_BIG } from '../../_consts';

export default () => {
  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;

  const $hero = $('.js-hero-austria');
  const $heroMain = $hero.find('.js-hero-austria-main');
  const $leftDecor = $hero.find('.js-hero-austria-left-decor');
  const $rightDecor = $hero.find('.js-hero-austria-right-decor');
  const $text = $hero.find('.js-hero-austria-text');

  const START_POS_Y = 40;
  const START_POS_X = 40;
  let endScrollXPos = 110;

  if (IS_TABLET && !IS_TABLET_BIG) {
    endScrollXPos = 150;
  }
  if (IS_PHONE) {
    endScrollXPos = 80;
  }

  const DURATION = IS_PHONE ? $heroMain.innerHeight() + 200 : $heroMain.innerHeight();

  const addScene = () => {
    const scrollTl = new TimelineMax();

    scrollTl
      .fromTo($leftDecor, 1,
        { x: 0, ease: Power0.easeNone },
        { x: endScrollXPos, ease: Power0.easeNone },
        0)
      .fromTo($rightDecor, 1,
        { x: 0, ease: Power0.easeNone },
        { x: -endScrollXPos, ease: Power0.easeNone },
        0)

    const scene = new ScrollMagic.Scene({
      triggerElement: $heroMain[0],
      triggerHook: 0,
      duration: DURATION,
      reverse: true,

    })
      // .addIndicators({ name: `hero` })
      .setTween(scrollTl)
      .addTo(controller);
    scenes.push(scene);

    setTimeout(() => {
      window.SCROLLER.scroll.start();
    }, 100);
  };

  const startCallback = () => { $hero.addClass(CLASSES.active) };
  const completeCallback = () => { addScene() };

  const tl = new TimelineMax({ onComplete: completeCallback, onStart: startCallback });
  tl
    .fromTo($leftDecor, 1,
      { x: -START_POS_X, y: -START_POS_Y, ease: Power3.out },
      { x: 0, y: 0, ease: Power3.out },
      0)
    .fromTo($rightDecor, 1,
      { x: START_POS_X, y: START_POS_Y, ease: Power3.out },
      { x: 0, y: 0, ease: Power3.out },
      0)
    .fromTo($text, 1,
      { y: START_POS_Y, ease: Power3.out },
      { y: 0, ease: Power3.out },
      0)
};
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import isPortrait from '../../helpers/_isPortrait';

import { DOM, IS_TOUCH, IS_PHONE, IS_TABLET, IS_TABLET_BIG }  from '../../_consts';

export default () => {
  const {controller} = window.SCROLLER;
  const {scenes} = window.SCROLLER;
  const $sections = $('.js-austria-scene-f');
  const IS_TABLET_OR_MOB = IS_TABLET && !IS_TABLET_BIG || IS_PHONE;
  
  $sections.each((i,el) => {
    const $section = $(el);
    const $leftCol = $section.find('.js-scene-left-col');
    const $animatedContent = $section.find('.js-scene-animated-content');
    const $bigImg = $section.find('.js-scene-big-img');
    const $caption = $section.find('.js-scene-caption');

    let sectionTl;
    let duration;
    const offset = 0;
    let leftColPaddingTop;
    let animatedContentHeight;
    let sectionHeight;
    let leftColEndPos;
    let startCaptionPos = -50;
    let endCaptionPos = 250;
    const startBigImgPos = -100;
    const endBigImgPos = 80;

    const calcValues = () => {
      const isPort = isPortrait();
      leftColPaddingTop = parseInt($leftCol.css('padding-top'), 10);
      animatedContentHeight = $animatedContent.innerHeight();
      sectionTl = new TimelineMax();
      sectionHeight = $leftCol.innerHeight();
      duration = sectionHeight;
      leftColEndPos = duration - animatedContentHeight - leftColPaddingTop;

      if(animatedContentHeight + endBigImgPos <= sectionHeight - leftColPaddingTop){
        leftColEndPos -= endBigImgPos;
      };

      if(IS_TABLET_OR_MOB){
        if(isPort){
          leftColEndPos = 0;
          startCaptionPos = -50;
          endCaptionPos = 250;
        }else{
          startCaptionPos = -50;
          endCaptionPos = 210;
        }
      };

      if(IS_PHONE){
        leftColEndPos = 0;
        if(isPort){
          startCaptionPos = -70;
          endCaptionPos = 200;
        }
        else{
          startCaptionPos = -50;
          endCaptionPos = 250;
        } 
      }
    };

    calcValues();

    const scene = new ScrollMagic.Scene({
      triggerElement: $leftCol[0],
      triggerHook: 0.5,
      duration,
      reverse: true,
      offset,
  
    });
    
    const prepareTimeline = () => {
      sectionTl
      .fromTo($caption, 1,
        { y: startCaptionPos, ease: Power0.easeNone },
        { y: endCaptionPos, ease: Power0.easeNone },
        0)
      .fromTo($bigImg, 1,
        { y: startBigImgPos, ease: Power0.easeNone },
        { y: endBigImgPos, ease: Power0.easeNone },
        0);

      if(!IS_PHONE){
        sectionTl.fromTo($animatedContent, 1,
          { y: 0, ease: Power0.easeNone },
          { y: leftColEndPos, ease: Power0.easeNone },
          0);

      }
    };

    prepareTimeline();
    
    const handleChangeScene = () => {
      sectionTl.stop();
      
      calcValues();
      prepareTimeline();

      scene.duration(duration);
      scene.setTween(sectionTl);
    };

    if(IS_TOUCH){
      DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
    }else{
      DOM.$win.on('resize', debounce(500, handleChangeScene));
    };

    scene
      .setTween(sectionTl)
      // .addIndicators({ name: `scene` })
      .addTo(controller);
    scenes.push(scene);
  })
};
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import isPortrait from '../../helpers/_isPortrait';

import { DOM, IS_TOUCH, IS_PHONE, IS_TABLET, IS_TABLET_BIG }  from '../../_consts';

export default () => {
  const {controller} = window.SCROLLER;
  const {scenes} = window.SCROLLER;
  const $sections = $('.js-austria-scene-g');
  const IS_TABLET_SMALL = IS_TABLET && !IS_TABLET_BIG;
  
  $sections.each((i,el) => {
    const $section = $(el);
    const $leftCol = $section.find('.js-scene-left-col');
    const $animatedContent = $section.find('.js-scene-animated-content');
    const $rowBlock = $section.find('.js-scene-block');

    let sectionTl;
    let duration;
    const offset = 0;
    let leftColPaddingTop;
    let animatedContentHeight;
    let sectionHeight;
    let leftColEndPos;
    let startRowPos = 130;
    let endRowPos = -70;

    const calcValues = () => {
      const isPort = isPortrait();
      leftColPaddingTop = parseInt($leftCol.css('padding-top'), 10);
      animatedContentHeight = $animatedContent.innerHeight();
      sectionTl = new TimelineMax();
      sectionHeight = $leftCol.innerHeight();
      duration = sectionHeight;
      leftColEndPos = duration - animatedContentHeight - leftColPaddingTop - endRowPos;

      if(IS_TABLET_SMALL && isPort){
        leftColEndPos = 0;
      };

      if(IS_PHONE){
        leftColEndPos = 0;
        if(isPort){
          startRowPos = 120;
          endRowPos = -80;
        }else{
          startRowPos = 50;
          endRowPos = -80;
        } 
      }
    };

    calcValues();

    const scene = new ScrollMagic.Scene({
      triggerElement: $leftCol[0],
      triggerHook: 0.5,
      duration,
      reverse: true,
      offset,
    });
    
    const prepareTimeline = () => {
      sectionTl
      .fromTo($rowBlock, 1,
        { y: startRowPos, ease: Power0.easeNone },
        { y: endRowPos, ease: Power0.easeNone },
        0)

      if(!IS_PHONE){
        sectionTl.fromTo($animatedContent, 1,
          { y: 0, ease: Power0.easeNone },
          { y: leftColEndPos, ease: Power0.easeNone },
          0);
      }
    };

    prepareTimeline();
    
    const handleChangeScene = () => {
      sectionTl.stop();
      
      calcValues();
      prepareTimeline();

      scene.duration(duration);
      scene.setTween(sectionTl);
    };

    if(IS_TOUCH){
      DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
    }else{
      DOM.$win.on('resize', debounce(500, handleChangeScene));
    };

    scene
      .setTween(sectionTl)
      // .addIndicators({ name: `scene` })
      .addTo(controller);
    scenes.push(scene);
  })
};
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import isPortrait from '../../helpers/_isPortrait';

import { DOM, IS_TOUCH, IS_PHONE, IS_TABLET,IS_TABLET_BIG } from '../../_consts';

export default () => {
  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;
  const $sections = $('.js-austria-scene-i');
  const IS_TABLET_SMALL = IS_TABLET && !IS_TABLET_BIG;

  $sections.each((i, el) => {
    const $section = $(el);
    const $leftCol = $section.find('.js-scene-left-col');
    const $animatedContent = $section.find('.js-scene-animated-content');
    const $caption = $section.find('.js-scene-caption');
    const $caption2 = $section.find('.js-scene-caption-2');
    const $smallImg = $section.find('.js-scene-small-img');
    let triggerHook = 0.4;

    if(IS_TABLET_SMALL){
      triggerHook = 0.5;
    }

    let sectionTl;
    let duration;
    const offset = 0;
    let leftColPaddingTop;
    let animatedContentHeight;
    let sectionHeight;
    let leftColEndPos;
    const startCaptionPos = 0;
    let endCaptionPos = -200;
    const startCaption2Pos = 100;
    let endCaption2Pos = -170;
    const startSmallImgPos = 150;
    const endSmallImgPos = -80;

    const calcValues = () => {
      const isPort = isPortrait();

      leftColPaddingTop = parseInt($leftCol.css('padding-top'), 10);
      animatedContentHeight = $animatedContent.innerHeight();
      sectionTl = new TimelineMax();
      sectionHeight = $leftCol.innerHeight();
      duration = sectionHeight;
      leftColEndPos = duration - animatedContentHeight - leftColPaddingTop;

      if(IS_PHONE){
        if(isPort){
          endCaptionPos = -200;
          endCaption2Pos = -100;
        }else{
          endCaptionPos = -250;
          endCaption2Pos = -170;
        }
      }


    };

    calcValues();

    const scene = new ScrollMagic.Scene({
      triggerElement: $leftCol[0],
      triggerHook,
      duration,
      reverse: true,
      offset,

    });

    const prepareTimeline = () => {
      sectionTl
        .fromTo($caption, 1,
          { y: startCaptionPos, ease: Power0.easeNone },
          { y: endCaptionPos, ease: Power0.easeNone },
          0)
        .fromTo($caption2, 1,
          { y: startCaption2Pos, ease: Power0.easeNone },
          { y: endCaption2Pos, ease: Power0.easeNone },
          0)
        .fromTo($smallImg, 1,
          { y: startSmallImgPos, ease: Power0.easeNone },
          { y: endSmallImgPos, ease: Power0.easeNone },
          0);

      if (!IS_PHONE) {
        sectionTl.fromTo($animatedContent, 1,
          { y: 0, ease: Power0.easeNone },
          { y: leftColEndPos, ease: Power0.easeNone },
          0);
      }
    };

    prepareTimeline();

    const handleChangeScene = () => {
      sectionTl.stop();

      calcValues();
      prepareTimeline();

      scene.duration(duration);
      scene.setTween(sectionTl);
    };

    if (IS_TOUCH) {
      DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
    } else {
      DOM.$win.on('resize', debounce(500, handleChangeScene));
    };

    scene
      .setTween(sectionTl)
      // .addIndicators({ name: `scene` })
      .addTo(controller);
    scenes.push(scene);
  })
};
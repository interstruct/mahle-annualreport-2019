import { debounce } from 'throttle-debounce';
import { DOM, CLASSES, IS_IE, IS_TOUCH } from "../../_consts";

class BackBtn {
  constructor() {
    this.$backToBlock = $('.js-back-to-block');
    this.$backToButton = $('.js-back-to-button');
    this.backHeight = 0;
    this.winHeight = 0;
    this.docHeight = 0;
    this.footerHeight = 0;
  }

  init() {
    if (!this.$backToButton[0] || IS_IE) return;
    setTimeout(() => {
      this.recountValues();

      this.$backToButton.on('click', () => {
        window.SCROLLER.scroll.scrollTo(this.$backToBlock[0]);
      });
  
      window.SCROLLER.scroll.on('scroll', this.onScroll.bind(this));

      if(IS_TOUCH){
        DOM.$win.on('orientationchange', debounce(500, this.recountValues.bind(this)));
      }else{
        DOM.$win.on('resize', debounce(100, this.recountValues.bind(this)));
      }
    }, 350);
  }

  recountValues() {
    this.backHeight = this.$backToButton.height();
    this.winHeight = DOM.$win.height();
    this.docHeight = $('.js-out').outerHeight();
    this.footerHeight = $('.js-footer').outerHeight();
  }

  hideButton() {
    this.$backToButton.removeClass(CLASSES.active);
  }

  showButton() {
    this.$backToButton.addClass(CLASSES.active);
  }

  stickButton(scrollTop) {
    this.$backToButton.css({
      'top': this.docHeight - this.footerHeight - this.winHeight - scrollTop
    });
  }

  unstickButton() {
    this.$backToButton.css({
      'top': 0
    });
  }

  onScroll(e) {    
    const st = e.scroll.y;
    if (st > this.winHeight / 2 + this.backHeight) {
      this.showButton();
    } else {
      this.hideButton();
    }
    if (st > this.docHeight - this.footerHeight - this.winHeight) {
      this.stickButton(st);
    } else {
      this.unstickButton();
    }
  }
}
const backToTopBtn = new BackBtn();

export default backToTopBtn;

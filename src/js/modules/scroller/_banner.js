import { TimelineMax, TweenMax, Power3 } from 'gsap';
import { debounce } from 'throttle-debounce';

import { CLASSES, DOM, IS_PHONE, IS_TOUCH, BREAKPOINTS } from '../../_consts';

export default () => {
  if (IS_PHONE) return;

  const $bannerWrap = $('.js-banner-wrap');
  const $banner = $bannerWrap.find('.js-banner');
  const $leftCol = $bannerWrap.find('.js-banner-left-col');
  const $closeBtn = $bannerWrap.find('.js-banner-close');
  
  const HIDE_TRANSFORM_X = 500;
  const CLOSED_TRANSFORM_X = -157;
  const OPEN_TRANSFORM_X = -980;
  const OPEN_TRANSFORM_X_LG = -750;
  const OPEN_TRANSFORM_X_MD = -650;
  const CLOSED_TRANSFORM_Y = 0;

  const HIDE_POS_Y = 500;
  
  let windowWidth;
  let windowHeight;
  let durationPX;
  let openTransformY = -200;
  let currentXpos;
  let currentYpos;
  let xPos;
  let yPos = 0;
  let isOpen = false;
  let isCanCLick = true;
  let isScrollAfterScene = false;
  let leftColHeight;
  let isHidden = false;
  let isShown = true;

  const calcXpos = () => {
    currentXpos = xPos;
    if (isOpen) {
      if (windowWidth > BREAKPOINTS.desktopL) {
        xPos = OPEN_TRANSFORM_X;
      } else {
        xPos = windowWidth <= BREAKPOINTS.tablet ? OPEN_TRANSFORM_X_MD : OPEN_TRANSFORM_X_LG;
      }
    } else {
      xPos = CLOSED_TRANSFORM_X;
    }
  };

  const calcYpos = () => {
    currentYpos = yPos;
    if (isOpen) {
      yPos = openTransformY - leftColHeight;
    } else {
      yPos = CLOSED_TRANSFORM_Y - leftColHeight;
    }
  };

  const calcXY = () => {
    calcXpos();
    calcYpos();
  };

  const calcValues = () => {
    windowWidth = window.innerWidth;
    windowHeight = window.innerHeight;
    openTransformY = windowHeight >= 670 ? -200 : -100;
    durationPX = windowHeight * 0.7;
    leftColHeight = $leftCol.innerHeight();
    calcXY();
  };
  calcValues();

  const disableClickTimeOut = (dur = 1000) => {
    setTimeout(() => {
      isCanCLick = true;
    }, dur)
  };

  const setClosingAnimation = (dur = 0.5) => {
    // eslint-disable-next-line
    const closingTl = new TimelineMax()
      .to($banner, dur,
        { x: xPos, y: yPos, ease: Power3.out }
      )
  };

  const setShowingAnimation = (dur = 0.5) => {
    // eslint-disable-next-line
    const showingTl = new TimelineMax({
      onComplete: () => {
        disableClickTimeOut(0)
      }
    })
      .to($banner, dur, { x: CLOSED_TRANSFORM_X, y: yPos, ease: Power3.out })
  };

  const setHidingAnimation = (dur = 1) => {
    // eslint-disable-next-line
    const hidingTl = new TimelineMax()
      .fromTo($banner, dur,
        { x: xPos, y: yPos, ease: Power3.out },
        { x: HIDE_TRANSFORM_X, y: HIDE_POS_Y, ease: Power3.out }
      )
  };

  const changeBannerState = (dur = 0.5) => {
    const y = currentYpos;

    if (isOpen) {
      TweenMax.fromTo($banner, dur,
        { x: currentXpos, y, ease: Power3.out },
        { x: xPos, y: yPos, ease: Power3.out }
      )
    } else if(!isOpen) {
       if (isScrollAfterScene) {
        isShown = false;
        isHidden = true;
        TweenMax.to($banner, 1, { x: HIDE_TRANSFORM_X, y: HIDE_POS_Y, ease: Power3.out })
      } else {
        setClosingAnimation();
      }
    }
  };

  const scrollHandle = (e) => {
    isScrollAfterScene = e.scroll.y > durationPX;
    if(isOpen) return;

    if(isScrollAfterScene){
      if(isHidden) return;
      isShown = false;
      isHidden = true;
      setHidingAnimation();
    }else{
      if(isShown) return
      isShown = true;
      isHidden = false;
      setShowingAnimation();
    }
  };

  const activeHandle = () => {
    if (!isCanCLick || $banner.hasClass(CLASSES.active)) return;

    isCanCLick = false;
    $banner.addClass(CLASSES.active);
    isOpen = true;
    calcXY();
    changeBannerState();
    disableClickTimeOut();
  };

  const closeHandle = (e) => {
    e.stopPropagation();

    if (!isCanCLick) return;
    isCanCLick = false;
    $banner.removeClass(CLASSES.active);
    isOpen = false;
    calcXY();
    changeBannerState();
    disableClickTimeOut();
  };

  $banner.on('click', activeHandle);
  $closeBtn.on('click', closeHandle);
  window.SCROLLER.scroll.on('scroll', scrollHandle);

  const resizeHandle = () => {
    calcValues();
    // $banner.css('transform', `translate3d(${xPos}px,${yPos}px, 0)`)
    window.SCROLLER.scroll.update();
  };
  
  if (IS_TOUCH) {
    DOM.$win.on('orientationchange', debounce(500, resizeHandle));
  } else {
    DOM.$win.on('resize', debounce(500, resizeHandle));
  };

  setTimeout(() => {
    TweenMax.fromTo($banner, 1,
      { x: CLOSED_TRANSFORM_X, y: -leftColHeight + 100, opacity: 0, ease: Power3.out },
      { x: CLOSED_TRANSFORM_X, y: -leftColHeight, opacity: 1, ease: Power3.out }
    )
  }, 100)
}
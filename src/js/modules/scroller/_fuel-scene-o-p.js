import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import isPortrait from '../../helpers/_isPortrait';

import { DOM, IS_TOUCH, IS_PHONE, IS_TABLET_SMALL} from '../../_consts';

export default () => {
  if(IS_PHONE) return;

  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;
  const $sections = $('.js-austria-scene-o, .js-austria-scene-p');

  $sections.each((i, el) => {
    const $section = $(el);
    const $leftCol = $section.find('.js-scene-left-col');
    const $quote = $section.find('.js-scene-quote');
    const $numberWrap = $section.find('.js-scene-number-wrap');
    const $number = $section.find('.js-scene-number');

    const NUBMER_TRIGGER_HOOK = 0.2;
    const HEADER_OFFSET = 110;
    const NUMBER_SCENE_OFFSET = 200;

    let isPort;
    let sectionTl;
    let duration;
    let quoteHeight;
    let quotePosTop;
    let quoteEndPos;
    let sectionHeight;

    let wrapperHeight;
    let numberScene;
    let numberDuration;
    let numberHeight;
    let endpos;
    let numberCurrentPos = 0;

    const calcNumberValues = () => {
      $numberWrap.css('height', `${sectionHeight}px`);

      numberDuration = duration + NUMBER_SCENE_OFFSET;
      wrapperHeight = $numberWrap.innerHeight();
      numberHeight = $number.innerHeight();
      endpos = wrapperHeight - numberHeight;
    };

    const calcValues = () => {
      isPort = isPortrait();
      $quote.removeAttr('style');

      quotePosTop = $quote.position().top;
      quoteHeight = $quote.innerHeight();
      sectionTl = new TimelineMax();
      sectionHeight = $leftCol.innerHeight();
      duration = sectionHeight;
      quoteEndPos = duration - quotePosTop - quoteHeight;

      if(IS_TABLET_SMALL){
        if(isPort){
          quoteEndPos = 0;
        }
      }
      calcNumberValues();
    };

    calcValues();

    const scene = new ScrollMagic.Scene({
      triggerElement: $leftCol[0],
      triggerHook: 0.5,
      duration,
      reverse: true,
    });

    const prepareTimeline = () => {
      sectionTl
        .fromTo($quote, 1,
          { y: 0, ease: Power0.easeNone },
          { y: quoteEndPos, ease: Power0.easeNone },
          0);
    };

    prepareTimeline();

    const handleChangeScene = () => {
      sectionTl.stop();

      calcValues();
      prepareTimeline();

      scene.duration(duration);
      scene.setTween(sectionTl);
      numberScene.duration(numberDuration);
    };

    if (IS_TOUCH) {
      DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
    } else {
      DOM.$win.on('resize', debounce(500, handleChangeScene));
    };

    scene
      .setTween(sectionTl)
      .addTo(controller);
    scenes.push(scene);

    const calcNumberPos = () => {
      const wrapperOffsetTop = $numberWrap.offset().top;
      const wrapperOffsetBottom = wrapperOffsetTop + wrapperHeight;
      if (wrapperOffsetTop < HEADER_OFFSET) {
        $number.addClass('is-active');
        if (wrapperOffsetTop >= 0) {
          numberCurrentPos = HEADER_OFFSET - Math.abs(wrapperOffsetTop);
          $number.removeClass('is-active');
        } else {
          numberCurrentPos = Math.abs(wrapperOffsetTop) + HEADER_OFFSET;
        }
      }
      if (wrapperOffsetBottom < numberHeight + HEADER_OFFSET) {
        numberCurrentPos = endpos;
      }
    };

    const initNumberScene = () => {
      numberScene = new ScrollMagic.Scene({
        triggerElement: $leftCol[0],
        triggerHook: NUBMER_TRIGGER_HOOK,
        duration: numberDuration,
        reverse: true,
        offset: -NUMBER_SCENE_OFFSET
      });

      numberScene.on('progress', () => {
        calcNumberPos();
        $number.css('transform', `translate3d(0,${numberCurrentPos}px, 0)`)
      })

      numberScene
        .addTo(controller);
      scenes.push(numberScene);
    }
    initNumberScene();
  })
};
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';


import { DOM, IS_TOUCH, IS_PHONE } from '../../_consts';

export default () => {
  if(IS_PHONE) return;

  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;
  const $sections = $('.js-austria-scene-q');

  $sections.each((i, el) => {
    const $section = $(el);
    const $leftCol = $section.find('.js-scene-left-col');
    const $numberWrap = $section.find('.js-scene-number-wrap');
    const $number = $section.find('.js-scene-number');

    const NUBMER_TRIGGER_HOOK = 0.2;
    const HEADER_OFFSET = 110;
    const NUMBER_SCENE_OFFSET = 200;

    let duration;
    let sectionHeight;

    let wrapperHeight;
    let numberScene;
    let numberDuration;
    let numberHeight;
    let endpos;
    let numberCurrentPos = 0;

    const calcValues = () => {
      sectionHeight = $leftCol.innerHeight();
      duration = sectionHeight;

      $numberWrap.css('height', `${sectionHeight}px`);

      numberDuration = duration + NUMBER_SCENE_OFFSET;
      wrapperHeight = $numberWrap.innerHeight();
      numberHeight = $number.innerHeight();
      endpos = wrapperHeight - numberHeight;
    };

    calcValues();

    const handleChangeScene = () => {
      calcValues();
      numberScene.duration(numberDuration);
    };


    const calcNumberPos = () => {
      const wrapperOffsetTop = $numberWrap.offset().top;
      const wrapperOffsetBottom = wrapperOffsetTop + wrapperHeight;
      if (wrapperOffsetTop < HEADER_OFFSET) {
        $number.addClass('is-active');
        if (wrapperOffsetTop >= 0) {
          numberCurrentPos = HEADER_OFFSET - Math.abs(wrapperOffsetTop);
          $number.removeClass('is-active');
        } else {
          numberCurrentPos = Math.abs(wrapperOffsetTop) + HEADER_OFFSET;
        }
      }
      if (wrapperOffsetBottom < numberHeight + HEADER_OFFSET) {
        numberCurrentPos = endpos;
      }
    };

    const initNumberScene = () => {
      numberScene = new ScrollMagic.Scene({
        triggerElement: $leftCol[0],
        triggerHook: NUBMER_TRIGGER_HOOK,
        duration: numberDuration,
        reverse: true,
        offset: -NUMBER_SCENE_OFFSET
      });

      numberScene.on('progress', () => {
        calcNumberPos();
        $number.css('transform', `translate3d(0,${numberCurrentPos}px, 0)`);
      })


      if (IS_TOUCH) {
        DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
      } else {
        DOM.$win.on('resize', debounce(500, handleChangeScene));
      };
  
      numberScene
        .addTo(controller);
      scenes.push(numberScene);
    }
    initNumberScene();
  })
};
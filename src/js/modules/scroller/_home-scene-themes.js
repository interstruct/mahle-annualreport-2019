import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import { DOM, IS_TOUCH, IS_PHONE } from '../../_consts';

export default () => {
  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;
  const $section = $('.js-home-themes');
  const $imgLeft = $section.find('.js-scene-left-img');
  const $imgRight = $section.find('.js-scene-right-img');

  let sectionTl;
  let duration;
  const leftImgStartPos = 0;
  let leftImgEndPos = -220;
  const rightImgStartPos = 0;
  let rightImgEndPos = 220;
  let offset;



  const calcValues = () => {
    const windowHeight = window.innerHeight;
    const blockHeight = $section.innerHeight();
    offset = windowHeight * 0.3;
    sectionTl = new TimelineMax();

    if (IS_PHONE) {
      offset = 0;
      leftImgEndPos = -130;
      rightImgEndPos = 130;
    };

    duration = windowHeight + blockHeight + offset;
  };

  calcValues();

  const scene = new ScrollMagic.Scene({
    triggerElement: $section[0],
    triggerHook: 1,
    duration,
    reverse: true,
    offset: -offset
  });

  const prepareTimeline = () => {
    sectionTl
      .fromTo($imgLeft, 1,
        { y: leftImgStartPos, ease: Power0.easeNone },
        { y: leftImgEndPos, ease: Power0.easeNone },
        0)
      .fromTo($imgRight, 1,
        { y: rightImgStartPos, ease: Power0.easeNone },
        { y: rightImgEndPos, ease: Power0.easeNone },
        0);
  };

  prepareTimeline();

  const handleChangeScene = () => {
    sectionTl.stop();

    calcValues();
    prepareTimeline();

    scene.duration(duration);
    scene.offset(-offset);
    scene.setTween(sectionTl);
  };

  if (IS_TOUCH) {
    DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
  } else {
    DOM.$win.on('resize', debounce(500, handleChangeScene));
  };

  scene
    .setTween(sectionTl)
    // .addIndicators({ name: `scene` })
    .addTo(controller);
  scenes.push(scene);
};
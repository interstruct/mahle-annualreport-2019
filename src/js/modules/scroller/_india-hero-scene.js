import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0, Power3 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';

import { CLASSES, IS_PHONE } from '../../_consts';

export default () => {
  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;
  // const $out = $('.js-out');
  const $hero = $('.js-hero-india');
  const $heroMain = $hero.find('.js-hero-india-main');
  const $img = $hero.find('.js-hero-img');
  const $rightMover = $hero.find('.js-hero-mask-wrapper, .js-hero-right-side');
  const $leftBgWrapper = $hero.find('.js-hero-left-bg-wrap');
  const $text = $hero.find('.js-hero-text');

  const IMG_END_POS = IS_PHONE ? 150: 250;
  const START_POS = 30;
  const DURATION = IS_PHONE ? $heroMain.innerHeight() + 150: $heroMain.innerHeight();
  const addScene = () => {
    const scrollTl = new TimelineMax();

    scrollTl
      .fromTo($rightMover, 1,
        { y: 0, ease: Power0.easeNone },
        { y: -80, ease: Power0.easeNone },
        0)
      .fromTo($img, 1,
        { y: 0, ease: Power0.easeNone },
        { y: IMG_END_POS, ease: Power0.easeNone },
        0)
    const scene = new ScrollMagic.Scene({
      triggerElement: $heroMain[0],
      triggerHook: 0,
      duration: DURATION,
      reverse: true,

    })
      // .addIndicators({ name: `india-hero` })
      .setTween(scrollTl)
      .addTo(controller);
    scenes.push(scene);

      setTimeout(() => {
        window.SCROLLER.scroll.start();
      }, 100);
  };
  if(IS_PHONE){
    $hero.addClass(CLASSES.active)
    addScene()
  }else{ 
    const startCallback = () => { $hero.addClass(CLASSES.active) };
    const completeCallback = () => { addScene() };
  
    const tl = new TimelineMax({ onComplete: completeCallback, onStart: startCallback });
    tl
      .fromTo($rightMover, 1,
        { y: START_POS, ease: Power3.out },
        { y: 0, ease: Power3.out },
        0)
      .fromTo($text, 1,
        { y: START_POS, ease: Power3.out },
        { y: 0, ease: Power3.out },
        0)
      .fromTo($img, 1,
        { y: -START_POS, ease: Power3.out },
        { y: 0, ease: Power3.out },
        0)
      .fromTo($leftBgWrapper, 1,
        { y: -START_POS, ease: Power3.out },
        { y: 0, ease: Power3.out },
        0);
  }
};
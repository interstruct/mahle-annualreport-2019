import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import isPortrait from '../../helpers/_isPortrait';

import { DOM, IS_TOUCH, IS_PHONE, IS_TABLET, IS_TABLET_BIG }  from '../../_consts';

export default () => {
  const {controller} = window.SCROLLER;
  const {scenes} = window.SCROLLER;
  const $sections = $('.js-india-scene-a');
  const IS_TABLET_SMALL = IS_TABLET && !IS_TABLET_BIG;
  
  $sections.each((i,el) => {
    const $section = $(el);
    const $leftCol = $section.find('.js-scene-left-col');
    const $animatedContent = $section.find('.js-scene-animated-content');
    const $caption = $section.find('.js-scene-caption');
    const $smallImg = $section.find('.js-scene-small-img');
    const $bigImg = $section.find('.js-scene-big-img');

    const $mainAnimatedElem = IS_PHONE ? $bigImg : $animatedContent;

    let sectionTl;
    let duration;
    const offset = 0;
    let leftColPaddingTop;
    let animatedContentHeight;
    let sectionHeight;
    let mainAnimatedElemEndPos;
    let startCaptionPos = 50;
    let endCaptionPos = 200;
    const startSmallImgPos = 150;
    const endSmallImgPos = -80;
    const triggerHook = IS_PHONE ? 0.7: 0.85;

    const calcValues = () => {
      leftColPaddingTop = parseInt($leftCol.css('padding-top'), 10);
      animatedContentHeight = $animatedContent.innerHeight();
      sectionTl = new TimelineMax();
      sectionHeight = $leftCol.innerHeight();
      duration = sectionHeight;
      mainAnimatedElemEndPos = duration - animatedContentHeight - leftColPaddingTop;

      if(IS_TABLET_SMALL){
        if(isPortrait()){
          mainAnimatedElemEndPos = 200;
          startCaptionPos = 150;
        }else{
          startCaptionPos = 50;
        }
      };


      if(IS_PHONE){
        startCaptionPos = 150;
        if(isPortrait()){
          endCaptionPos = 350;
          mainAnimatedElemEndPos = 150;
          duration = 500
        }else{
          endCaptionPos = 350;
        }
      };
    };

    calcValues();

    const scene = new ScrollMagic.Scene({
      triggerElement: $leftCol[0],
      triggerHook,
      duration,
      reverse: true,
      offset,
  
    });
    
    const prepareTimeline = () => {
      sectionTl
      .fromTo($caption, 1,
        { y: startCaptionPos, ease: Power0.easeNone },
        { y: endCaptionPos, ease: Power0.easeNone },
        0)
      .fromTo($mainAnimatedElem, 1,
        { y: 0, ease: Power0.easeNone },
        { y: mainAnimatedElemEndPos, ease: Power0.easeNone },
        0);

        if(!IS_PHONE){
          sectionTl.fromTo($smallImg, 1,
            { y: startSmallImgPos, ease: Power0.easeNone },
            { y: endSmallImgPos, ease: Power0.easeNone },
            0);
        };
    };

    prepareTimeline();
    
    const handleChangeScene = () => {
      sectionTl.stop();
      
      calcValues();
      prepareTimeline();

      scene.duration(duration);
      scene.setTween(sectionTl);
    };

    if(IS_TOUCH){
      DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
    }else{
      DOM.$win.on('resize', debounce(500, handleChangeScene));
    };

    scene
      .setTween(sectionTl)
      // .addIndicators({ name: `india-scene a` })
      .addTo(controller);
    scenes.push(scene);
  })
};
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import {DOM, IS_TOUCH, IS_PHONE }  from '../../_consts';

export default () => {
  const {controller} = window.SCROLLER;
  const {scenes} = window.SCROLLER;
  const $sections = $('.js-india-scene-d');
  
  if(IS_PHONE) return;

  $sections.each((i,el) => {
    const $section = $(el);
    const $leftCol = $section.find('.js-scene-left-col');
    const $animatedContent = $section.find('.js-scene-animated-content');
    const $quote = $section.find('.js-scene-quote');
    const $bigImg = $section.find('.js-scene-big-img');
    
    let leftColPaddingTop;
    let animatedContentHeight;
    let sectionTl;
    let sectionHeight;
    let duration;
    let endPos;


    const calcMainValues = () => {
      leftColPaddingTop = parseInt($leftCol.css('padding-top'), 10);
      animatedContentHeight = $animatedContent.innerHeight();
      sectionTl = new TimelineMax();
      sectionHeight = $leftCol.innerHeight();
      duration = sectionHeight;
      endPos = duration - animatedContentHeight - leftColPaddingTop;
    };

    calcMainValues()

    const prepareTimeline = () => {
      sectionTl
      .fromTo($quote, 1,
        { y: 0, ease: Power0.easeNone },
        { y: endPos, ease: Power0.easeNone },
        0)
      .fromTo($bigImg, 1,
        { y: 0, ease: Power0.easeNone },
        { y: endPos, ease: Power0.easeNone },
        0)
    };

    const scene = new ScrollMagic.Scene({
      triggerElement: $leftCol[0],
      triggerHook: 0.45,
      duration,
      reverse: true,
  
    });
    prepareTimeline()

    const handleChangeScene = () => {
      sectionTl.stop();
      calcMainValues();
      prepareTimeline();
      scene.duration(duration);
      scene.setTween(sectionTl);
      // scene.refresh();
    };

    if(IS_TOUCH){
      DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
    }else{
      DOM.$win.on('resize', debounce(500, handleChangeScene));
    };

    scene
      .setTween(sectionTl)
      // .addIndicators({ name: `india-scene d` })
      .addTo(controller);
    scenes.push(scene);
  })
};
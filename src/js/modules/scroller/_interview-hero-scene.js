import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power3 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';

import { CLASSES, IS_PHONE } from '../../_consts';

export default () => {
  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;

  const $hero = $('.js-hero-interview');
  const $heroMain = $hero.find('.js-hero-interview-main');
  const $leftImg = $hero.find('.js-scene-first-img');
  const $rightImg = $hero.find('.js-scene-second-img');
  const $leftDecor = $hero.find('.js-scene-left-decor');
  const $rightDecor = $hero.find('.js-scene-right-decor');
  const $text = $hero.find('.js-hero-text');

  const START_POS = 50;
  const START_IMG_POS = 30;
  let leftImgScrollEndPos = 100;
  let rightImgScrollEndPos = 100;
  let leftDerocScrollEndPos = 70;

  if(IS_PHONE){
    leftImgScrollEndPos = 70;
    rightImgScrollEndPos = 70;
    leftDerocScrollEndPos = 70;
  }

  const DURATION = IS_PHONE ? $heroMain.innerHeight() + 150 : $heroMain.innerHeight();
  const addScene = () => {
    const scrollTl = new TimelineMax();

    scrollTl
    .fromTo($leftDecor, 1,
      { y: 0, ease: Power3.out },
      { y: leftDerocScrollEndPos, ease: Power3.out },
      0)
    .fromTo($rightDecor, 1,
      { y: 0, ease: Power3.out },
      { y: -leftDerocScrollEndPos, ease: Power3.out },
      0)
    .fromTo($leftImg, 1,
      { y: 0, ease: Power3.out },
      { y: leftImgScrollEndPos, ease: Power3.out },
      0)
    .fromTo($rightImg, 1,
      { y: 0, ease: Power3.out },
      { y: rightImgScrollEndPos, ease: Power3.out },
      0)
    const scene = new ScrollMagic.Scene({
      triggerElement: $heroMain[0],
      triggerHook: 0,
      duration: DURATION,
      reverse: true,

    })
      // .addIndicators({ name: `hero` })
      .setTween(scrollTl)
      .addTo(controller);
    scenes.push(scene);

    setTimeout(() => {
      window.SCROLLER.scroll.start();
    }, 100);
  };
  if (IS_PHONE) {
    $hero.addClass(CLASSES.active)
    addScene()
  } else {
    const startCallback = () => { $hero.addClass(CLASSES.active) };
    const completeCallback = () => {
      addScene()
    };

    const tl = new TimelineMax({ onComplete: completeCallback, onStart: startCallback });
    tl
      .fromTo($leftDecor, 1,
        { y: -START_POS, ease: Power3.out },
        { y: 0, ease: Power3.out },
        0)
      .fromTo($rightDecor, 1,
        { y: START_POS, ease: Power3.out },
        { y: 0, ease: Power3.out },
        0)
      .fromTo($leftImg, 1,
        { y: -START_IMG_POS, ease: Power3.out },
        { y: 0, ease: Power3.out },
        0)
      .fromTo($rightImg, 1,
        { y: START_IMG_POS, ease: Power3.out },
        { y: 0, ease: Power3.out },
        0)
      .fromTo($text, 1,
        { y: START_POS, ease: Power3.out },
        { y: 0, ease: Power3.out },
        0);
  }
};
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import isPortrait from '../../helpers/_isPortrait';

import { DOM, IS_TOUCH, IS_PHONE, IS_PHONE_SMALL, IS_TABLET } from '../../_consts';

export default () => {
  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;
  const $section = $('.js-puzzle-scene');

  const $puzzleWrap = $section.find('.js-gsap-puzzle');
  const $puzzleLeft = $puzzleWrap.find('.js-gsap-puzzle-left');
  const $puzzleRight = $puzzleWrap.find('.js-gsap-puzzle-right');
  const $puzzleCaption = $section.find('.js-scene-caption');

  let isPort;
  let sectionTl;
  let duration;
  const leftImgStartPos = -40;
  const leftImgEndPos = 0;
  const rightImgStartPos = 40;
  const rightImgEndPos = 0;
  let captionStartPos = -50;
  let captionEndPos = -250;
  let offset = 0;
  let puzzleTime = 1;

  const calcValues = () => {
    const windowHeight = window.innerHeight;
    const blockHeight = $puzzleWrap.innerHeight();
    isPort = isPortrait();
    sectionTl = new TimelineMax();
    duration = windowHeight >= blockHeight ? windowHeight * 0.9 : blockHeight;
    offset = 0;

    if (IS_TABLET) {
      captionEndPos = -200;
      if (isPort) {
        duration = windowHeight * 0.7;
      } else {
        duration = windowHeight * 0.85;
      }
    };

    if (IS_PHONE) {
      duration = windowHeight > blockHeight ? windowHeight * 0.8 : blockHeight;
      puzzleTime = windowHeight > 760 ? 0.8 : 0.4;
      if (isPort) {
        duration *= 1;
        captionStartPos = 50;
        captionEndPos = -100;
      }

      if (!isPort && !IS_PHONE_SMALL) {
        captionStartPos = -50;
        captionEndPos = -250;
      }
    }
  };

  calcValues();

  const scene = new ScrollMagic.Scene({
    triggerElement: $puzzleWrap[0],
    triggerHook: 1,
    duration,
    reverse: true,
    offset
  });

  const prepareTimeline = () => {
    sectionTl
      .fromTo($puzzleLeft, puzzleTime,
        { y: leftImgStartPos, ease: Power0.easeNone },
        { y: leftImgEndPos, ease: Power0.easeNone },
        0)
      .fromTo($puzzleRight, puzzleTime,
        { y: rightImgStartPos, ease: Power0.easeNone },
        { y: rightImgEndPos, ease: Power0.easeNone },
        0);

    sectionTl
      .fromTo($puzzleCaption, puzzleTime,
        { y: captionStartPos, ease: Power0.easeNone },
        { y: captionEndPos, ease: Power0.easeNone },
        0);

  };

  prepareTimeline();

  const handleChangeScene = () => {
    sectionTl.stop();

    calcValues();
    prepareTimeline();

    scene.duration(duration);
    scene.setTween(sectionTl);
  };

  if (IS_TOUCH) {
    DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
  } else {
    DOM.$win.on('resize', debounce(500, handleChangeScene));
  };

  scene
    .setTween(sectionTl)
    // .addIndicators({ name: `scene` })
    .addTo(controller);
  scenes.push(scene);
};
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0, Power3 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';

import { CLASSES, IS_PHONE } from '../../_consts';

export default () => {
  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;

  const $hero = $('.js-hero-italy');
  const $heroMain = $hero.find('.js-hero-italy-main');
  const $leftDecor = $hero.find('.js-hero-italy-left-decor');
  const $rightDecor = $hero.find('.js-hero-italy-right-decor');
  const $leftImg = $hero.find('.js-hero-img-left-wrapper').find('img');
  const $rightImg = $hero.find('.js-hero-img-right-wrapper').find('img');
  const $text = $hero.find('.js-hero-italy-text');

  const START_POS_Y = 40;
  const START_POS_X = 50;

  const leftImgStartPos = 70;
  const leftImgEndPos = 0;
  const rightImgStartPos = -70;
  const rightImgEndPos = 0;

  let endScrollXPos = 150;
  let endScrollImgXPos = 90;

  if (IS_PHONE) {
    endScrollXPos = 100;
    endScrollImgXPos = 60;
  }

  const DURATION = IS_PHONE ? $heroMain.innerHeight() + 200 : $heroMain.innerHeight();

  const addScene = () => {
    const scrollTl = new TimelineMax();

    scrollTl
      .fromTo($leftDecor, 1,
        { x: 0, ease: Power0.easeNone },
        { x: endScrollXPos, ease: Power0.easeNone },
        0)
      .fromTo($rightDecor, 1,
        { x: 0, ease: Power0.easeNone },
        { x: -endScrollXPos, ease: Power0.easeNone },
        0)
      .fromTo($leftImg, 1,
        { y: 0, ease: Power0.easeNone },
        { y: -endScrollImgXPos, ease: Power0.easeNone},
        0)
      .fromTo($rightImg, 1,
        { y: 0, ease: Power0.easeNone },
        { y: endScrollImgXPos, ease: Power0.easeNone },
        0)
    const scene = new ScrollMagic.Scene({
      triggerElement: $heroMain[0],
      triggerHook: 0,
      duration: DURATION,
      reverse: true,

    })
      // .addIndicators({ name: `hero` })
      .setTween(scrollTl)
      .addTo(controller);
    scenes.push(scene);

    setTimeout(() => {
      window.SCROLLER.scroll.start();
    }, 100);
  };

  const startCallback = () => { $hero.addClass(CLASSES.active) };
  const completeCallback = () => { addScene() };

  const tl = new TimelineMax({ onComplete: completeCallback, onStart: startCallback });
  tl
    .fromTo($leftDecor, 1,
      { x: -START_POS_X, ease: Power3.out },
      { x: 0, ease: Power3.out },
      0)
    .fromTo($rightDecor, 1,
      { x: START_POS_X, ease: Power3.out },
      { x: 0, ease: Power3.out },
      0)
    .fromTo($leftImg, 1,
      { y: leftImgStartPos, ease: Power3.out },
      { y: leftImgEndPos, ease: Power3.out },
      0)
    .fromTo($rightImg, 1,
      { y: rightImgStartPos, ease: Power3.out },
      { y: rightImgEndPos, ease: Power3.out },
      0)
    .fromTo($text, 1,
      { y: START_POS_Y, ease: Power3.out },
      { y: 0, ease: Power3.out },
      0)
};
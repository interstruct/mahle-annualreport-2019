import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import isPortrait from '../../helpers/_isPortrait';

import { DOM, IS_TOUCH, IS_PHONE, IS_PHONE_SMALL, IS_TABLET } from '../../_consts';

export default () => {
  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;
  const $section = $('.js-italy-scene-k');

  const $puzzleWrap = $section.find('.js-gsap-puzzle');
  const $puzzleLeft = $puzzleWrap.find('.js-gsap-puzzle-left');
  const $puzzleRight = $puzzleWrap.find('.js-gsap-puzzle-right');
  const $puzzleCaptionLeft = $puzzleWrap.find('.js-scene-caption-left');
  const $puzzleCaptionRight = $puzzleWrap.find('.js-scene-caption-right');


  let isPort;
  let sectionTl;
  let duration;
  let leftImgStartPos = 0;
  let leftImgEndPos = -40;
  let rightImgStartPos = 0;
  let rightImgEndPos = 40;
  let captionStartPos = 50;
  let captionEndPos = 200;
  let puzzleTime = 1;
  let windowHeight;


  const calcValues = () => {
    windowHeight = window.innerHeight;
    const blockHeight = $puzzleWrap.innerHeight();
    isPort = isPortrait();
    sectionTl = new TimelineMax();
    duration = windowHeight >= blockHeight ? windowHeight : blockHeight;

    if (window.innerWidth < 1200) {
      captionStartPos = 0;
      captionEndPos = 100;
    };

    if (IS_TABLET) {
      if (isPort) {
        duration = windowHeight * 0.7;
      } else {
        duration = windowHeight * 0.85;
      }
    };

    if (IS_PHONE) {
      duration = windowHeight > blockHeight ? windowHeight * 0.7 : blockHeight;
      puzzleTime = windowHeight > 760 ? 0.8 : 0.4;
      leftImgStartPos = -40;
      leftImgEndPos = 0;
      rightImgStartPos = 40;
      rightImgEndPos = 0;

      if (!isPort && !IS_PHONE_SMALL) {
        duration *= 0.8;
        puzzleTime = 1;
        leftImgStartPos = 0;
        leftImgEndPos = -40;
        rightImgStartPos = 0;
        rightImgEndPos = 40;
      }
    }
  };

  calcValues();

  const scene = new ScrollMagic.Scene({
    triggerElement: $puzzleWrap[0],
    triggerHook: 0.8,
    duration,
    reverse: true,
  });

  const prepareTimeline = () => {
    sectionTl
      .fromTo($puzzleLeft, puzzleTime,
        { y: leftImgStartPos, ease: Power0.easeNone },
        { y: leftImgEndPos, ease: Power0.easeNone },
        0)
      .fromTo($puzzleRight, puzzleTime,
        { y: rightImgStartPos, ease: Power0.easeNone },
        { y: rightImgEndPos, ease: Power0.easeNone },
        0);

    if (!IS_PHONE_SMALL) {
      sectionTl
        .fromTo($puzzleCaptionLeft, 1,
          { y: captionStartPos, ease: Power0.easeNone },
          { y: captionEndPos, ease: Power0.easeNone },
          0)
        .fromTo($puzzleCaptionRight, 1,
          { y: -captionStartPos, ease: Power0.easeNone },
          { y: -captionEndPos, ease: Power0.easeNone },
          0);
    }
  };

  prepareTimeline();

  const handleChangeScene = () => {
    sectionTl.stop();

    calcValues();
    prepareTimeline();

    scene.duration(duration);
    scene.setTween(sectionTl);
  };

  if (IS_TOUCH) {
    DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
  } else {
    DOM.$win.on('resize', debounce(500, handleChangeScene));
  };

  scene
    .setTween(sectionTl)
    // .addIndicators({ name: `scene` })
    .addTo(controller);
  scenes.push(scene);
};
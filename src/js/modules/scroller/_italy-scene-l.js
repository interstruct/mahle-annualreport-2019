import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import isPortrait from '../../helpers/_isPortrait';

import { DOM, IS_TOUCH, IS_PHONE, IS_TABLET_SMALL } from '../../_consts';

export default () => {

  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;
  const $sections = $('.js-italy-scene-l');

  $sections.each((i, el) => {
    const $section = $(el);
    const $leftCol = $section.find('.js-scene-left-col');
    const $animatedContent = $section.find('.js-scene-animated-content');
    const $quote = $section.find('.js-scene-quote');
    let isPort;
    let sectionTl;
    let duration;
    let leftColPaddingTop;
    let animatedContentHeight;
    let sectionHeight;
    let leftColEndPos;
    const quoteStartPos = 0;
    const quoteEndPos = 70;

    const calcValues = () => {
      isPort = isPortrait();
      leftColPaddingTop = parseInt($leftCol.css('padding-top'), 10);
      animatedContentHeight = $animatedContent.innerHeight();
      sectionTl = new TimelineMax();
      sectionHeight = $leftCol.innerHeight();
      duration = sectionHeight;
      leftColEndPos = duration - animatedContentHeight - leftColPaddingTop;
    };

    calcValues();

    const scene = new ScrollMagic.Scene({
      triggerElement: $leftCol[0],
      triggerHook: 0.5,
      duration,
      reverse: true,
    });

    const prepareTimeline = () => {

      if (IS_TABLET_SMALL && isPort || IS_PHONE) {
        sectionTl.fromTo($quote, 1,
          { y: quoteStartPos, ease: Power0.easeNone },
          { y: quoteEndPos, ease: Power0.easeNone },
          0);
      } else {
        sectionTl.fromTo($animatedContent, 1,
          { y: 0, ease: Power0.easeNone },
          { y: leftColEndPos, ease: Power0.easeNone },
          0);
      }
    };

    prepareTimeline();

    const handleChangeScene = () => {
      sectionTl.stop();
      $animatedContent.removeAttr('style');

      calcValues();
      prepareTimeline();

      scene.duration(duration);
      scene.setTween(sectionTl);
    };

    if (IS_TOUCH) {
      DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
    } else {
      DOM.$win.on('resize', debounce(500, handleChangeScene));
    };

    scene
      .setTween(sectionTl)
      // .addIndicators({ name: `scene` })
      .addTo(controller);
    scenes.push(scene);
  })
};
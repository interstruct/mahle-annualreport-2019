import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import isPortrait from '../../helpers/_isPortrait';

import { DOM, IS_TOUCH, IS_PHONE, IS_PHONE_SMALL, IS_TABLET_SMALL } from '../../_consts';

export default () => {

  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;
  const $sections = $('.js-italy-scene-m');

  $sections.each((i, el) => {
    const $section = $(el);
    const $leftCol = $section.find('.js-scene-left-col');
    const $animatedContent = $section.find('.js-scene-animated-content');
    const $puzzleWrap = $section.find('.js-gsap-puzzle');
    const $puzzleLeft = $puzzleWrap.find('.js-gsap-puzzle-left');
    const $puzzleRight = $puzzleWrap.find('.js-gsap-puzzle-right');

    let sectionTl;
    let duration;
    const triggerHook = IS_PHONE ? 0.8: 0.6;
    let leftColPaddingTop;
    let animatedContentHeight;
    let sectionHeight;
    let leftColEndPos;
    const leftImgStartPos = -40;
    const rightImgStartPos = 40;
    let puzzleTime = 0.45;
    let puzzleHeight;
    let windowHeight;

    const calcValues = () => {
      const isPort = isPortrait();
      windowHeight = window.innerHeight;
      leftColPaddingTop = parseInt($leftCol.css('padding-top'), 10);
      animatedContentHeight = $animatedContent.innerHeight();
      sectionTl = new TimelineMax();
      sectionHeight = $leftCol.innerHeight();
      duration = sectionHeight;
      puzzleHeight = $puzzleWrap.innerHeight()
      leftColEndPos = duration - animatedContentHeight - leftColPaddingTop;

      if(IS_TABLET_SMALL){
        if(isPort){
          puzzleTime = 0.4;
        }else{
          puzzleTime = 0.45;
        }
      };

      if(IS_PHONE){
        duration = windowHeight > puzzleHeight ? windowHeight*0.7 : puzzleHeight;
        puzzleTime = windowHeight > 760 ? 0.8: 0.4;
      }
    };

    calcValues();

    const scene = new ScrollMagic.Scene({
      triggerElement: $leftCol[0],
      triggerHook,
      duration,
      reverse: true,
    });

    const prepareTimeline = () => {
      sectionTl
        .fromTo($puzzleLeft, puzzleTime,
          { y: leftImgStartPos, ease: Power0.easeNone },
          { y: 0, ease: Power0.easeNone },
          0)
        .fromTo($puzzleRight, puzzleTime,
          { y: rightImgStartPos, ease: Power0.easeNone },
          { y: 0, ease: Power0.easeNone },
          0);

      if (!IS_PHONE_SMALL) {
        sectionTl
          .fromTo($animatedContent, 1,
            { y: 0, ease: Power0.easeNone },
            { y: leftColEndPos, ease: Power0.easeNone },
            0);
      }
    };

    prepareTimeline();

    const handleChangeScene = () => {
      sectionTl.stop();
      $animatedContent.removeAttr('style');

      calcValues();
      prepareTimeline();

      scene.duration(duration);
      scene.setTween(sectionTl);
    };

    if (IS_TOUCH) {
      DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
    } else {
      DOM.$win.on('resize', debounce(500, handleChangeScene));
    };

    scene
      .setTween(sectionTl)
      .addTo(controller);
    scenes.push(scene);
  })
};
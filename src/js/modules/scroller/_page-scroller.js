import LocomotiveScroll from 'locomotive-scroll';
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';
import updateViewportUnits from '../../helpers/_updateViewportUnits';



import isPortrait from '../../helpers/_isPortrait';
import { DOM, IS_TOUCH, IS_PHONE, IS_PHONE_BIG, IS_TABLET, IS_TABLET_BIG } from '../../_consts';

class Scroller {
  constructor() {
    this.$container = $('.js-page-scroll-container');
    this.$sizeChangers = $('.js-click-update-scroll');
    this.$locoElems = $('.js-loco-elem');
    this.isPortrait = isPortrait();
    this.stopScrollOnInit = this.$container.attr('data-stop-scroll-on-init') === 'true';
    this.scenes = [];
    this.disableAttrs = {
      lTabletDisable: 'data-l-tablet-disable',
      pTabletDisable: 'data-p-tablet-disable',
      lMobDisable: 'data-l-mob-disable',
      lMobBigDisable: 'data-l-mob-big-disable',
      pMobDisable: 'data-p-mob-disable',
    }
    this.scrollAttrs = [
      {
        default: 'data-scroll-speed',
        lTablet: 'data-l-tablet-speed',
        pTablet: 'data-p-tablet-speed',
        lMob: 'data-l-mob-speed',
        lMobBig: 'data-l-mob-big-speed',
        pMob: 'data-p-mob-speed',
      },
      {
        default: 'data-scroll-direction',
        lTablet: 'data-l-tablet-direction',
        pTablet: 'data-p-tablet-direction',
        lMob: 'data-l-mob-direction',
        lMobBig: 'data-l-mob-big-direction',
        pMob: 'data-p-mob-direction',
      }

    ];
    this.jsAttrs = [];

    this.init();
  }

  init() {
    if(!this.$container[0]) return;

      if(IS_TOUCH && IS_TABLET_BIG === false){
        this.checkAnimationChanges();
      }
      // if(!IS_IE){
        this.initScroll();
        this.setEvents();
      // }

    this.initController();
  }

  initController(){
    // if(IS_IE){
    //   this.controller = new ScrollMagic.Controller();
    // }else{
      this.controller = new ScrollMagic.Controller({refreshInterval: 0});
    // }
  }

  initScroll(){
    const inertia = IS_TOUCH ? 0.5 : 1; // do not replace to init object

    const props = {
      el: this.$container[0],
      smooth: true,
      smoothMobile: true,
      touchMultiplier: 5,
      getDirection: true,
      inertia
    };

     this.scroll = new LocomotiveScroll(props);

     this.scroll.stop();
  }

  setPortraitAttrs(){
    const disableAttr = IS_PHONE ? this.disableAttrs.pMobDisable : this.disableAttrs.pTabletDisable;
    const key = IS_PHONE ? 'pMob' : 'pTablet';

    this.setAttrsLoop(disableAttr, key);
  }

  setLandscapeAttrs(){
    let disableAttr = IS_PHONE ? this.disableAttrs.lMobDisable : this.disableAttrs.pTabletDisable;
    let key = IS_PHONE ? 'lMob' : 'lTablet';

    if(IS_PHONE_BIG){
      disableAttr = this.disableAttrs.lMobBigDisable;
      key = 'lMobBig';
    };

    this.setAttrsLoop(disableAttr, key);
  }

  setAttrsLoop(disableAttr, key){
    this.$locoElems.each((i, element) => {
      const $el = $(element);
      const isDisable = $el.attr(disableAttr) === 'true';
      if(isDisable){
        this.scrollAttrs.forEach(obj => {
          $el.removeAttr(obj.default);
          $el.removeAttr('style');
        });

      }else{
        $el.attr('data-scroll', '')
        this.scrollAttrs.forEach(obj => {
          const generatedValue = $el.attr(obj[key]);
          if(generatedValue !== undefined){
            $el.attr(`${obj.default}`, `${generatedValue}`)
          }
        });
      }
    });
  }

  checkAnimationChanges(){
    const isPortraitTablet = !!(IS_TABLET && this.isPortrait);
    const isPortraitPhone = !!(IS_PHONE && this.isPortrait);
    if(isPortraitTablet || isPortraitPhone){
      this.setPortraitAttrs();
    }else{
      this.setLandscapeAttrs();
    }
  };

  refreshScenes(){
    this.scenes.forEach(element => {
      element.refresh();
    });
  }

  setEvents(){
    this.scroll.on('scroll', () => {this.refreshScenes();});

    const onResize = () => {
      updateViewportUnits();
      this.scroll.update();

      if(IS_TOUCH && IS_TABLET_BIG === false){
        this.isPortrait = isPortrait();
        this.checkAnimationChanges();
        this.scroll.update();
      }
    };
    
    DOM.$win.on('resize orientationchange', debounce(500, onResize));

    if(!this.stopScrollOnInit){
      setTimeout(() => {
        this.scroll.start();
      }, 700)
    }
  }
};

DOM.$win.ready(() => {
  window.SCROLLER = new Scroller();
});




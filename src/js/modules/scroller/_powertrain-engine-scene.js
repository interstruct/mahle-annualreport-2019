import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import isPortrait from '../../helpers/_isPortrait';

import { DOM, IS_TOUCH, IS_PHONE, IS_PHONE_SMALL } from '../../_consts';

export default () => {
  if (IS_PHONE_SMALL) return;

  const { controller } = window.SCROLLER;
  const { scenes } = window.SCROLLER;

  const $section = $('.js-engine-section');
  const $container = $section.find('.js-scene-container');
  const $imgWrap = $section.find('.js-scene-img');
  const $img = $imgWrap.find('img');

  const NUBMER_TRIGGER_HOOK = 0.2;
  const HEADER_OFFSET = 110;

  let isPortPhone;
  let sectionHeight;
  let wrapperHeight;
  let imgTopOffset;
  let scene;
  let duration;
  let imgHeight;
  let endpos;
  let numberCurrentPos = 0;

  isPortPhone = isPortrait() && IS_PHONE;

  const calcValues = () => {

    sectionHeight = $container.innerHeight();

    if(!isPortPhone){
      $imgWrap.css('height', `${sectionHeight}px`);
    }

    duration = $section.innerHeight();
    wrapperHeight = $imgWrap.innerHeight() + 120;
    imgTopOffset = parseInt($img.css('top'), 10);
    imgHeight = $img.innerHeight();
    endpos = wrapperHeight - imgHeight - imgTopOffset;

  };

  calcValues();

  const handleChangeScene = () => {
    isPortPhone = isPortrait() && IS_PHONE;
    $imgWrap.removeAttr('style');
    $img.removeAttr('style');

    if (isPortPhone) return;
    calcValues();
    scene.duration(duration);
  };


  const calcPos = () => {
    const wrapperOffsetTop = $imgWrap.offset().top + imgTopOffset;
    const wrapperOffsetBottom = wrapperOffsetTop + wrapperHeight;
    numberCurrentPos = 0;
    if (wrapperOffsetTop < HEADER_OFFSET) {
      if (wrapperOffsetTop >= 0) {
        numberCurrentPos = HEADER_OFFSET - Math.abs(wrapperOffsetTop);
      } else {
        numberCurrentPos = Math.abs(wrapperOffsetTop) + HEADER_OFFSET;
      }
    }
    if (wrapperOffsetBottom < imgHeight + imgTopOffset + HEADER_OFFSET) {
      numberCurrentPos = endpos;
    }
  };

  scene = new ScrollMagic.Scene({
    triggerElement: $section[0],
    triggerHook: NUBMER_TRIGGER_HOOK,
    duration,
    reverse: true,
  });

  scene.on('progress', () => {
    if (isPortPhone) return;
    
    calcPos();
    $img.css('transform', `translate3d(0,${numberCurrentPos}px, 0)`);
  })


  if (IS_TOUCH) {
    DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
  } else {
    DOM.$win.on('resize', debounce(500, handleChangeScene));
  };

  scene
    // .addIndicators({ name: `engine scene` })
    .addTo(controller);
  scenes.push(scene);
};
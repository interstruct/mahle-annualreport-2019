
const initDefauiltScrollTo = () => {
  const scrollTo = e => {
    const targetSectionName = $(e.currentTarget).data('section');
    const $targetSection = $(`#${targetSectionName}`);
    const delta = $('.js-header').outerHeight();
    window.SCROLLER.scroll.scrollTo($targetSection[0], -delta);
  };

  $('.js-scroll-to').on('click', scrollTo);
};

export default initDefauiltScrollTo;
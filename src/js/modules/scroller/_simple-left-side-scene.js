import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax, Power0 } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import { debounce } from 'throttle-debounce';

import isPortrait from '../../helpers/_isPortrait';

import { DOM, IS_TOUCH, IS_PHONE, IS_TABLET_SMALL }  from '../../_consts';

export default () => {
  if(IS_PHONE) return;

  const {controller} = window.SCROLLER;
  const {scenes} = window.SCROLLER;
  const $sections = $('.js-simple-left-side-scene');
  
  $sections.each((i,el) => {
    const $section = $(el);
    const $leftCol = $section.find('.js-scene-left-col');
    const $animatedContent = $section.find('.js-scene-animated-content');

    let sectionTl;
    let duration;
    const offset = 0;
    let leftColPaddingTop;
    let animatedContentHeight;
    let sectionHeight;
    let leftColEndPos;

    const calcValues = () => {
      leftColPaddingTop = parseInt($leftCol.css('padding-top'), 10);
      animatedContentHeight = $animatedContent.innerHeight();
      sectionTl = new TimelineMax();
      sectionHeight = $leftCol.innerHeight();
      duration = sectionHeight;
      leftColEndPos = duration - animatedContentHeight - leftColPaddingTop;
    };

    calcValues();

    const scene = new ScrollMagic.Scene({
      triggerElement: $leftCol[0],
      triggerHook: 0.5,
      duration,
      reverse: true,
      offset,
  
    });
    
    const prepareTimeline = () => {

      if(isPortrait() && IS_TABLET_SMALL){
        $animatedContent.removeAttr('style')
      }else{
        sectionTl
        .fromTo($animatedContent, 1,
          { y: 0, ease: Power0.easeNone },
          { y: leftColEndPos, ease: Power0.easeNone },
          0);
      }
      }

    prepareTimeline();
    
    const handleChangeScene = () => {
      sectionTl.stop();

      
      calcValues();
      prepareTimeline();

      scene.duration(duration);
      scene.setTween(sectionTl);
    };

    if(IS_TOUCH){
      DOM.$win.on('orientationchange', debounce(500, handleChangeScene));
    }else{
      DOM.$win.on('resize', debounce(500, handleChangeScene));
    };

    scene
      .setTween(sectionTl)
      // .addIndicators({ name: `scene` })
      .addTo(controller);
    scenes.push(scene);
  })
};
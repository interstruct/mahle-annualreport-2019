import initDropdowns from './modules/_dropdowns';
import backToTopBtn from './modules/scroller/_back-to-top';
import initHorizontalSlider from './modules/_horizontal-slider';
import initHeader from './modules/_header';
import initHeroScene from './modules/scroller/_powertrain-hero-scene';
import initSimpleLeftSideScene from './modules/scroller/_simple-left-side-scene';
import initEngineScene from './modules/scroller/_powertrain-engine-scene';
import initScheme from './modules/_powertrain-scheme';
import initModals from './modules/_modal';
import initBanner from './modules/scroller/_banner';
import { initPageNavigation } from './modules/_page-navigation';

import { DOM } from './_consts';

import './modules/scroller/_page-scroller';

const initModules = () => {
  initHeader();
  initBanner();
  backToTopBtn.init();
  initPageNavigation();
  
  initDropdowns();
  initHorizontalSlider();
  initScheme();
  initModals();
};

const initScenes = () => {
  initHeroScene();
  initSimpleLeftSideScene();
  initEngineScene();
};

const readyHandle = () => {
  initModules();
  initScenes();
};

DOM.$win.ready(readyHandle);

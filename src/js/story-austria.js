import initDropdowns from './modules/_dropdowns';
import backToTopBtn from './modules/scroller/_back-to-top';
import initHorizontalSlider from './modules/_horizontal-slider';
import initHeader from './modules/_header';
import initHeroScene from './modules/scroller/_austria-hero-scene';
import initSceneE from './modules/scroller/_austria-scene-e';
import initSceneF from './modules/scroller/_austria-scene-f';
import initSceneG from './modules/scroller/_austria-scene-gg';
import initSceneH from './modules/scroller/_austria-scene-h';
import initSceneI from './modules/scroller/_austria-scene-i';
import initModals from './modules/_modal';
import initBanner from './modules/scroller/_banner';
import { initPageNavigation } from './modules/_page-navigation';

import { DOM } from './_consts';

import './modules/scroller/_page-scroller';

const initModules = () => {
  initHeader();
  initBanner();
  backToTopBtn.init();
  initPageNavigation();
  
  initDropdowns();
  initHorizontalSlider();
  initModals();
};

const initScenes = () => {
  initHeroScene();
  initSceneE();
  initSceneF();
  initSceneG();
  initSceneH();
  initSceneI();
};

const readyHandle = () => {
  initModules();
  initScenes();
};

DOM.$win.ready(readyHandle);

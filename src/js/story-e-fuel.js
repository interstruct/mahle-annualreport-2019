import initDropdowns from './modules/_dropdowns';
import backToTopBtn from './modules/scroller/_back-to-top';
import initHorizontalSlider from './modules/_horizontal-slider';
import initHeader from './modules/_header';
import initHeroScene from './modules/scroller/_fuel-hero-scene';
import initSceneN from './modules/scroller/_fuel-scene-n';
import initSceneOP from './modules/scroller/_fuel-scene-o-p';
import initSceneQ from './modules/scroller/_fuel-scene-q';
import initModals from './modules/_modal';
import initBanner from './modules/scroller/_banner';
import { initPageNavigation } from './modules/_page-navigation';

import { DOM } from './_consts';

import './modules/scroller/_page-scroller';

const initModules = () => {
  initHeader();
  initBanner();
  backToTopBtn.init();
  initPageNavigation();

  initDropdowns();
  initHorizontalSlider();
  initModals();
};

const initScenes = () => {
  initHeroScene();
  initSceneN();
  initSceneOP();
  initSceneQ();
};

const readyHandle = () => {
  initModules();
  initScenes();
};

DOM.$win.ready(readyHandle);

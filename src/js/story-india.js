import initDropdowns from './modules/_dropdowns';
import backToTopBtn from './modules/scroller/_back-to-top';
import initHorizontalSlider from './modules/_horizontal-slider';
import initHeader from './modules/_header';
import initHeroScene from './modules/scroller/_india-hero-scene';
import initSceneA from './modules/scroller/_india-scene-a';
import initSceneB from './modules/scroller/_india-scene-b';
import initSceneC from './modules/scroller/_india-scene-c';
import initSceneD from './modules/scroller/_india-scene-d';
import initModals from './modules/_modal';
import initBanner from './modules/scroller/_banner';
import { initPageNavigation } from './modules/_page-navigation';

import { DOM } from './_consts';

import './modules/scroller/_page-scroller';

const initModules = () => {
  initHeader();
  initBanner();
  backToTopBtn.init();
  initPageNavigation();
  
  initDropdowns();
  initHorizontalSlider();
  initModals();
};

const initScenes = () => {
  // initNavigationScene();
  initHeroScene();
  initSceneA();
  initSceneB();
  initSceneC();
  initSceneD();
};

const readyHandle = () => {
  initModules();
  initScenes();
};

DOM.$win.ready(readyHandle);

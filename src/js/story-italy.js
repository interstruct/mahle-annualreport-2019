import initDropdowns from './modules/_dropdowns';
import backToTopBtn from './modules/scroller/_back-to-top';
import initHorizontalSlider from './modules/_horizontal-slider';
import initHeader from './modules/_header';
import initHeroScene from './modules/scroller/_italy-hero-scene';
import initSceneJ from './modules/scroller/_italy-scene-j';
import initSimpleLeftSideScene from './modules/scroller/_simple-left-side-scene';
import initSceneKPuzzle from './modules/scroller/_italy-scene-k-puzzle';
import initSceneL from './modules/scroller/_italy-scene-l';
import initSceneM from './modules/scroller/_italy-scene-m';
import initModals from './modules/_modal';
import initTooltips from './modules/_tooltips';
import initBanner from './modules/scroller/_banner';
import { initPageNavigation } from './modules/_page-navigation';

import { DOM } from './_consts';

import './modules/scroller/_page-scroller';

const initModules = () => {
  initHeader();
  initBanner();
  backToTopBtn.init();
  initPageNavigation();
  
  initDropdowns();
  initHorizontalSlider();
  initTooltips();
  initModals();
};

const initScenes = () => {
  initHeroScene();
  initSceneJ();
  initSimpleLeftSideScene();
  initSceneKPuzzle();
  initSceneL();
  initSceneM();
};

const readyHandle = () => {
  initModules();
  initScenes();
};

DOM.$win.ready(readyHandle);

import backToTopBtn from './modules/scroller/_back-to-top';
import initHeader from './modules/_header';

import { DOM } from './_consts';

import './modules/scroller/_page-scroller';

const initModules = () => {
  initHeader();
  backToTopBtn.init();
};

const readyHandle = () => {
  initModules();
};

DOM.$win.ready(readyHandle);
